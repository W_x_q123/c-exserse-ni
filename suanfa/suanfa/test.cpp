#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <vector>
#include <queue>

using namespace std;


struct ListNode {
    int val;
    struct ListNode* next;
    ListNode(int x) : val(x), next(nullptr) {}

};


ListNode* oddEvenList(ListNode* head) {
    // write code here
    ListNode* first = nullptr, * second = nullptr;
    ListNode* cur = head;
    ListNode* ret = new ListNode(0);
    if (cur->val % 2 != 0)
        first = cur;
    else
        second = cur;
    ret->next = cur;
    cur = cur->next;
    int n = 1;
    while (cur != nullptr)
    {
        if (cur->val % 2 != 0)
        {
            if (first == nullptr)
            {
                second->next = cur;
                first = cur;
            }
            else
            {
                ListNode* next = first->next;
                ListNode* newnode = new ListNode(cur->val);
                first->next = newnode;
                newnode->next = next;
                first = first->next;
            }
        }
        else
        {
            if (second == nullptr)
            {
                first->next = cur;
                second = cur;
            }
            else
            {
                ListNode* next = second->next;
                ListNode* newnode = new ListNode(cur->val);
                second->next = newnode;
                newnode->next = next;
                second = second->next;
            }
        }
        cur = cur->next;
        n++;
    }
    cur = ret->next;
    n--;
    while (n--)
    {
        cur = cur->next;
    }
    cur ->next= nullptr;
    return ret->next;
}


//int main()
//{
//    ListNode* n1 = new  ListNode(1);
//    ListNode* n2 = new  ListNode(2);
//    ListNode* n3 = new  ListNode(3);
//    ListNode* n4 = new  ListNode(4);
//    ListNode* n5 = new  ListNode(5);
//    ListNode* n6 = new  ListNode(6);
//    
//    n1->next = n2;
//    n2->next = n3;
//    n3->next = n4;
//    n4->next = n5;
//    n5->next = n6;
//
//    ListNode*ret=oddEvenList(n1);
//    while (ret != nullptr)
//    {
//        cout << ret->val << " ";
//        ret = ret->next;
//    }
//
//    return 0;
//
//
//}


//int main()
//{
//    string str="0.1";
//    cout << stoi(str) << endl;
//
//    return 0;
//}


struct TreeNode {
    int val;
    struct TreeNode* left;
    struct TreeNode* right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};


vector<int> getPath(TreeNode* root, int t)
{
    vector<int> path;
    TreeNode* cur = root;
    queue<TreeNode*> q;
    q.push(cur);
    bool flag = false;
    while (!q.empty())
    {
        int sz = q.size();
        for (int i = 0; i < sz; i++)
        {
            TreeNode* top = q.front();
            q.pop();
            if (top->val == t)
            {
                break;
                flag = true;
            }
            path.push_back(top->val);
            if (top->left)
                q.push(top->left);
            if (top->right)
                q.push(top->right);
        }
        if (flag)
            break;
    }

    path.push_back(t);
    return path;
}

int lowestCommonAncestor(TreeNode* root, int o1, int o2) {
    // write code here
    vector<int> p1 = getPath(root, o1);
    vector<int> p2 = getPath(root, o2);
    for (auto v : p1)
        cout << v << " ";
    cout << endl;
    for (auto v : p2)
        cout << v << " ";
    cout << endl;

    int ret;
    for (int i = 0; i < p1.size() && i < p2.size(); i++)
    {
        if (p1[i] == p2[i])
            ret = p1[i];
        else
            break;
    }

    return ret;
}

//int main()
//{
//    TreeNode* root = new TreeNode(3);
//    TreeNode* n1 = new TreeNode(5);
//    TreeNode* n2 = new TreeNode(1);
//    TreeNode* n3 = new TreeNode(6);
//    TreeNode* n4 = new TreeNode(2);
//    TreeNode* n5 = new TreeNode(0);
//    TreeNode* n6 = new TreeNode(8);
//    TreeNode* n7 = new TreeNode(7);
//    TreeNode* n8 = new TreeNode(4);
//
//    root->left = n1; root->right = n2;
//    n1->left = n3; n1->right = n4;
//    n3->left = nullptr; n3->right = nullptr;
//    n4->left = n7; n4->right = n8;
//    n7->left = nullptr; n7->right = nullptr;
//    n8->left = nullptr; n8->right = nullptr;
//    n2->left = n5; n2->right = n6;
//    n5->left = nullptr; n5->right = nullptr;
//    n6->left = nullptr; n6->right = nullptr;
//
//    int ret = lowestCommonAncestor(root, 2, 7);
//
//    cout << "ret: " << ret << endl;
//
//    return 0;
//
//
//}


//int main()
//{
//    /*int val = 150;
//    char tmp[5] = { 0 };
//    sprintf(tmp, "%d", val);
//    string ret;
//    ret += tmp;
//    cout << "ret: " << ret << endl;
//    cout << &ret[0] << endl;
//    cout << *(&ret[0]) << endl;*/
//    /*string ret;
//    ret += val;
//    cout << "ret: " << ret << endl;*/
//
//   const  char* str = "qwer#1234";
//   cout << sizeof(str) << endl;
//   cout << strlen(str) << endl;
//  
//
//    return 0;
//}


char* Serialize(TreeNode* root) {
    queue<TreeNode*> q;
    q.push(root);
    string ret;
    while (!q.empty())
    {
        int sz = q.size();
        for (int i = 0; i < sz; i++)
        {
            TreeNode* top = q.front();
            q.pop();
            if (top == nullptr)
            {
                ret += '#';
                continue;
            }
            
            char tmp[5] = { 0 };
            sprintf(tmp, "%d", top->val);
            ret += tmp;
            
             q.push(top->left);
             q.push(top->right);
            

        }
    }
    int n = 0;
    for (int i = ret.size() - 1; i >= 0; i--)
    {
        if (ret[i] == '#')
            n++;
        else
            break;
    }

    string str = ret.substr(0, ret.size() - n);
    return &str[0];
}

//int main()
//{
//    TreeNode* root = new TreeNode(1);
//    TreeNode* n1 = new TreeNode(2);
//    TreeNode* n2 = new TreeNode(3);
//    TreeNode* n3 = new TreeNode(6);
//    TreeNode* n4 = new TreeNode(7);
//
//    root->left = n1; root->right = n2;
//    n1->left = nullptr; n1->right = nullptr;
//    n2->left = n3; n2->right = n4;
//    n3->left = nullptr; n3->right = nullptr;
//    n4->left = nullptr; n4->right = nullptr;
//
//    cout << Serialize(root) << endl;
//
//    return 0;
//}
//
//
////int main()
//{
//    string ret = "123##67####";
//
//    int n = 0;
//    for (int i = ret.size() - 1; i >= 0; i--)
//    {
//        if (ret[i] == '#')
//            n++;
//        else
//            break;
//    }
//
//    string str = ret.substr(0, ret.size()-n);
//
//    cout << str << endl;
//
//    return 0;
//}


TreeNode* build(vector<int>& pre, vector<int>& in)
{
    if (pre.size() == 0 || in.size() == 0)
        return nullptr;

    TreeNode* root = new TreeNode(pre[0]);
    for (int i = 0; i < in.size(); i++)
    {
        if (pre[0] == in[i])
        {
            vector<int> leftpre(pre.begin() + 1, pre.begin() + i + 1);
            vector<int> leftin(in.begin(), in.begin() + i);
            root->left = build(leftpre, leftin);

            vector<int> rightpre(pre.begin() + i + 1, pre.end());
            vector<int> rightin(in.begin() + i + 1, in.end());
            root->right = build(rightpre, rightin);
            break;
        }
    }
    return root;
}
vector<int> solve(vector<int>& pre, vector<int>& in) {
    // write code here
    queue<TreeNode*> q;
    TreeNode* root = build(pre, in);
    q.push(root);
    vector<int> ret;
    ret.push_back(root->val);
    while (!q.empty())
    {
        int sz = q.size();
        for (int i = 0; i < sz; i++)
        {
            TreeNode* top = q.front();
            q.pop();
            if (top->left)
                q.push(top->left);
            if (top->right)
            {
                ret.push_back(top->right->val);
                q.push(top->right);
            }
        }
    }
    return ret;
}

int main()
{
    vector<int> pre = { 1,2,4,5,3 };
    vector<int> in = { 4,2,5,1,3 };

    solve(pre, in);

    return 0;
}
