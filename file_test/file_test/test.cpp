#include <iostream>
#include <fstream>


using namespace std;


int main()
{
	string out_path = "test1.txt";
	fstream out(out_path, ios::out);
	if (!out.is_open())
	{
		cout << "open error" << endl;
		return 1;
	}
	for (int i = 1; i <= 100; i++)
	{
		out << i << " ";
		if (i % 10 == 0)
			out << endl;
	}

	out.close();

	ifstream in(out_path, ios::in);
	if (!in.is_open())
	{
		cout << "in error" << endl;
		return 1;
	}

	//string str;
	char str[10];
	int cnt = 0;
	//while (!in.eof())
	while (in.getline(str, sizeof(str)))
	{
		//in >> str;
		cout << "in getline" << endl;
		cout << str << " ";
		cnt++;
		if (cnt % 10 == 0)
			cout << endl;
	}

	return 0;

}