#include<algorithm>
#include<iostream>
#include <vector>
#include <stack>
using namespace std;


//// 约瑟夫问题的函数实现  
//// n: 人数  
//// m: 报数到m的人出列  
//vector<int> josephus(int n, int m) {
//    vector<int> people; // 存储人的编号的容器  
//    for (int i = 1; i <= n; ++i) {
//        people.push_back(i); // 初始化人的编号，从1到n  
//    }
//
//    vector<int> result; // 存储出列顺序的容器  
//    int index = 0; // 当前报数到的人的位置  
//
//    while (!people.empty()) {
//        // 找到报数到m的人的位置  
//        index = (index + m - 1) % people.size(); // 减1是因为要从index开始数，而不是从index+1开始  
//
//        // 将出列的人添加到结果容器中  
//        result.push_back(people[index]);
//
//        // 移除出列的人  
//        people.erase(people.begin() + index);
//    }
//
//    return result;
//}
//
//int main() {
//    int n, m;
//    cout << "请输入人数n和报数到m的人出列：";
//    cin >> n >> m;
//
//    vector<int> result = josephus(n, m);
//
//    cout << "出列顺序为：";
//    for (int i : result) {
//        cout << i << " ";
//    }
//    cout << endl;
//
//    return 0;
//}





//class TreeNode
//{
//public:
//	int val;
//	TreeNode* left;
//	TreeNode* right;
//
//	TreeNode(int n)
//		:val(n),left(nullptrptr),right(nullptrptr)
//	{}
//};
//
//class Tree
//{
//public:
//	Tree()
//		:root(nullptrptr)
//	{}
//
//	void CreateTree(int n,vector<int>nums)
//	{
//		root = new TreeNode(nums[0]);
//		TreeNode* tmp = root;
//		for (int i = 1; i < nums.size(); i++)
//		{
//			tmp->left = new TreeNode(nums[i]);
//			tmp->right = new TreeNode(nums[i+1]);
//			
//		}
//	}
//public:
//	TreeNode* root;
//};





//class TreeNode {
//public:
//    int val;
//    TreeNode* left;
//    TreeNode* right;
//
//    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//};
//
//class BinaryTree {
//public:
//    TreeNode* root;
//
//    BinaryTree() : root(nullptr) {}
//
//    void insert(int val) {
//        if (root == nullptr) {
//            root = new TreeNode(val);
//        }
//        else {
//            insertHelper(root, val);
//        }
//    }
//
//private:
//    void insertHelper(TreeNode* node, int val) 
//    {
//        if (val < node->val) 
//        {
//            if (node->left == nullptr) {
//                node->left = new TreeNode(val);
//            }
//            else {
//                insertHelper(node->left, val);
//            }
//        }
//        else {
//            if (node->right == nullptr) {
//                node->right = new TreeNode(val);
//            }
//            else {
//                insertHelper(node->right, val);
//            }
//        }
//    }
//};
//
//int main() {
//    BinaryTree tree;
//    tree.insert(5);
//    tree.insert(3);
//    tree.insert(7);
//    tree.insert(2);
//    tree.insert(4);
//    tree.insert(6);
//    tree.insert(8);
//
//    cout << "前序遍历结果：" << endl;
//    cout << tree.root->val << " ";
//    cout << tree.root->left->val << " ";
//    cout << tree.root->right->val << " ";
//    cout << tree.root->left->left->val << " ";
//    cout << tree.root->left->right->val << " ";
//    cout << tree.root->right->left->val << " ";
//    cout << tree.root->right->right->val << endl;
//
//    return 0;
//}



//typedef int Tdatatype;
//
//typedef struct Tree
//{
//	Tdatatype data;
//	struct Tree* left;
//	struct Tree* right;
//}Tree;
//
//Tree* BuyTree(Tdatatype x)
//{
//	Tree* node = (Tree*)malloc(sizeof(Tree));
//	if (node == nullptr)
//	{
//		perror("malloc fail");
//		return nullptr;
//	}
//	node->data = x;
//	node->left = nullptr;
//	node->right = nullptr;
//
//	return node;
//}
//
//Tree* CreateTree()    
//{
//	Tree* node1 = BuyTree(1);
//	Tree* node2 = BuyTree(2);
//	Tree* node3 = BuyTree(3);
//	Tree* node4 = BuyTree(4);
//	Tree* node5 = BuyTree(5);
//	Tree* node6 = BuyTree(6);
//	Tree* node7 = BuyTree(7);
//	node1->left = node2;
//	node1->right = node4;
//	node2->left = node3;
//	node2->right = node7;
//	node4->left = node5;
//	node4->right = node6;
//
//	return node1;
//
//}
//
//
//void InOrder(Tree* root)
//{
//	if (root == nullptr)
//		return;
//
//	InOrder(root->left);
//	cout << root->data << " ";
//	InOrder(root->right);
//}
//
//void InorderR(Tree* root) {
//	stack<Tree*> stk;
//	Tree* cur = root;
//
//	while (cur != nullptr || !stk.empty()) {
//		while (cur != nullptr) {
//			stk.push(cur);
//			cur = cur->left;
//		}
//		cur = stk.top();
//		stk.pop();
//		cout << cur->data << " ";
//		cur = cur->right;
//	}
//}
//
//int main()
//{
//	Tree* root=CreateTree();
//	cout << "递归方式中序遍历二叉树:" << endl;
//	InOrder(root);
//	cout << endl << endl;
//	cout << "非递归方式中序遍历二叉树:" << endl;
//	InorderR(root);
//	cout << endl;
//
//	return 0;
//}





#include <iostream>
#include <vector>
#include <list>

using namespace std;

// 图的节点（用于邻接表）
class GraphNode {
public:
    int vertex; // 节点编号
    list<int> adjList; // 邻接表

    GraphNode(int v) : vertex(v) {}
};

// 图的类
class Graph 
{
private:
    vector<GraphNode*> graph; // 图的邻接表表示
    bool* visited; // 标记节点是否已访问

public:
    Graph(int V) 
    {
        graph.resize(V);
        visited = new bool[V];
        for (int i = 0; i < V; i++) 
        {
            graph[i] = new GraphNode(i);
            visited[i] = false;
        }
    }

    ~Graph() 
    {
        for (auto node : graph) 
        {
            delete node;
        }
        delete[] visited;
    }

    // 添加边
    void addEdge(int v, int w) 
    {
        graph[v]->adjList.push_back(w);
        // 无向图的话，需要添加反向边
        // graph[w]->adjList.push_back(v);
    }

    // 深度优先遍历
    void DFS(int v) 
    {
        visited[v] = true;
        cout << v << " ";

        for (auto it = graph[v]->adjList.begin(); it != graph[v]->adjList.end(); ++it) 
        {
            int w = *it;
            if (!visited[w]) 
            {
                DFS(w);
            }
        }
    }
};

int main() {
    // 创建一个有5个节点的图
    Graph g(5);

    // 添加边
    g.addEdge(0, 1);
    g.addEdge(0, 4);
    g.addEdge(1, 2);
    g.addEdge(1, 3);
    g.addEdge(1, 4);
    g.addEdge(2, 3);
    g.addEdge(3, 4);

    // 从节点0开始深度优先遍历
    cout << "DFS starting from vertex 0: " << endl;
    g.DFS(0);
    cout << endl;

    return 0;
}
