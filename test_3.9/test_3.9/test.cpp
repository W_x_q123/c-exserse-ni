#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>


using namespace std;


//void Sum(vector<int>& nums, int pos, int sum, vector<int>& ret_sum)
//{
//	if (pos == nums.size())
//	{
//		ret_sum.push_back(sum);
//		return;
//	}
//
//	Sum(nums, pos + 1, sum, ret_sum);
//	Sum(nums, pos + 1, sum + nums[pos], ret_sum);
//}


//int getFirstUnFormedNum(vector<int> arr, int len)
//{
//	int max = 0, min = INT_MAX;
//	for (auto num : arr)
//	{
//		max += num;
//		if (num < min)
//			min = num;
//	}
//
//	vector<int> hash(max);
//	for (int i = min; i <= max; i++)
//	{
//		hash[i]++;
//	}
//
//	vector<int> ret_sum;
//	Sum(arr, 0, 0, ret_sum);
//	cout << "ret_sum size: " << ret_sum.size() << endl;
//	for (auto e : ret_sum)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//	// for(auto e:ret_sum)
//	// {
//	// 	hash[e]--;
//	// }
//
//	// for(int i=min;i<=max;i++)
//	// {
//	// 	if(hash[i]==1)
//	// 		return i;
//	// }
//
//	return max + 1;
//
//}


//int main()
//{
//	vector<int> arr = { 3,2,5 };
//	vector<int> ret;
//
//	int max = 0, Min = INT_MAX;
//	for (auto num : arr)
//	{
//		max += num;
//		if (num < Min)
//			Min = num;
//	}
//
//	cout << "min: " << Min << " max: " << max << endl;
//	/*Sum(arr, 0, 0, ret);
//
//	for (auto e : ret)
//		cout << e << " ";
//	cout << endl;*/
//
//	return 0;
//}


void Sum(vector<int>& nums, int pos, int sum, vector<int>& ret_sum)
{
	if (pos == nums.size())
	{
		ret_sum.push_back(sum);
		return;
	}

	Sum(nums, pos + 1, sum, ret_sum);
	Sum(nums, pos + 1, sum + nums[pos], ret_sum);
}


bool isexist(vector<int>& ret_sum, int target)
{
	unordered_map<int, int> hash;
	for (int i = 0; i < ret_sum.size(); i++)
	{
		if (hash.count(target - ret_sum[i]))
			return true;
		hash[ret_sum[i]]++;
	}

	return false;
}

int getFirstUnFormedNum(vector<int> arr, int len)
{
	int max = 0, Min = INT_MAX;
	for (auto num : arr)
	{
		max += num;
		if (num < Min)
			Min = num;
	}

	cout << "min: " << Min << "max: " << max << endl;
	vector<int> ret_sum;
	Sum(arr, 0, 0, ret_sum);

	int ret = INT_MAX;
	for (int i = Min; i <= max; i++)
	{
		if (!isexist(ret_sum, i))
			ret = min(ret, i);
	}

	if (ret == INT_MAX)
		return max + 1;
	else
		return ret;

}


//int main()
//{
//	vector<int> arr = { 3,1,2 };
//	cout << getFirstUnFormedNum(arr, arr.size()) << endl;
//
//	return 0;
//}


int dx[4] = { 0,0,-1,1 };
int dy[4] = { 1,-1,0,0, };

int m, n;

void bfs(vector<vector<int>>&hash,int i, int j)
{

    for (int k = 0; k < 4; k++)
    {
        int x = i + dx[k], y = j + dy[k];
        if (x >= 0 && x < m && y >= 0 && y < n && hash[x][y] == 0)
        {
            hash[x][y] = 1;
            bfs(hash,x, y);
        }
    }
}

int numIslands(vector<vector<char>>& grid)
{
    int _m = grid.size(), _n = grid[0].size();
    m = _m, n = _n;

    vector<vector<int>> hash(_m, vector<int>(_n));
   
    int ret = 0;
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (grid[i][j] == '1' && hash[i][j] == 0)
            {
                ret++;
                hash[i][j] = 1;
                bfs(hash,i, j);
            }
        }
    }

    return ret;
}


int main()
{
	vector<vector<char>> num = { {1,1,1,0},{1,1,0,0},{1,0,0,0} };

	cout << numIslands(num) << endl;

	return 0;
}


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


//string multiply(string num1, string num2)
//{
//    string ret;
//    int m = num1.size(), n = num2.size();
//    vector<int> vs(m + n - 1);
//    reverse(num1.begin(), num1.end());
//    reverse(num2.begin(), num2.end());
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//        }
//    }
//
//    int t = 0;
//    for (int i = 0; i < vs.size(); i++)
//    {
//        ret += to_string((vs[i] + t) % 10);
//        t = (vs[i] + t) / 10;
//    }
//    reverse(ret.begin(), ret.end());
//
//    return ret;
//}
//
//int main()
//{
//	string n1 = "123";
//	string n2 = "456";
//
//    string ret = multiply(n1, n2);
//
//		cout << ret << endl;
//		return 0;
//}
//
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        //无进位相乘，然后相加，最后处理进位
//        if (strcmp(num1.c_str(), "0") == 0 || strcmp(num2.c_str(), "0") == 0)  //处理前导0
//            return "0";
//        string ret;
//        int m = num1.size(), n = num2.size();
//        vector<int> vs(m + n - 1);
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//
//        int t = 0;
//        for (int i = 0; i < vs.size(); i++)
//        {
//            ret += to_string((vs[i] + t) % 10);
//            t = (vs[i] + t) / 10;
//        }
//
//        if (t != 0)
//            ret += to_string(t);
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    string addBinary(string a, string b)
//    {
//        int t = 0;
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        while (i >= 0 || j >= 0 || t != 0)
//        {
//            if (i >= 0)
//            {
//                t = t + (a[i] - '0');
//                i--;
//            }
//
//            if (j >= 0)
//            {
//                t = t + (b[j] - '0');
//                j--;
//            }
//
//            ret += to_string(t % 2);
//            t /= 2;
//        }
//
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    string removeDuplicates(string s)
//    {
//
//        string ret;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (ret.size() != 0 && ret.back() == s[i])
//                ret.pop_back();
//            else
//                ret += s[i];
//        }
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string str1, str2;
//        int i = 0, j = 0;
//        while (i < s.size() || j < t.size())
//        {
//            if (i < s.size())
//            {
//                if (s[i] == '#')
//                {
//                    if (str1.size())
//                        str1.pop_back();
//                }
//                else
//                    str1 += s[j];
//            }
//
//            if (j < t.size())
//            {
//                if (t[j] == '#')
//                {
//                    if (str2.size())
//                        str2.pop_back();
//                }
//                else
//                    str2 += t[j];
//            }
//            i++;
//            j++;
//        }
//
//        if (strcmp(str1.c_str(), str2.c_str()) == 0)
//            return true;
//        else
//            return false;
//    }
//};



//string decodeString(string s)
//{
//    stack<int> num;
//    stack<char> let_bra;
//    string ret;
//    int flag = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s[i] >= '0' && s[i] <= '9')
//            num.push(s[i] - '0');
//        else if (s[i] >= 'a' && s[i] <= 'z')
//        {
//            if (flag == 0)
//            {
//                ret += s[i];
//            }
//            else if (flag == 1)
//                let_bra.push(s[i]);
//        }
//        else if (s[i] == '[')
//        {
//            flag = 1;
//            let_bra.push(s[i]);
//        }
//        else if (s[i] == ']')
//        {
//            flag = 0;
//            string tmp;
//            while (let_bra.top() != '[')
//            {
//                tmp += let_bra.top();
//                let_bra.pop();
//            }
//            reverse(tmp.begin(), tmp.end());
//            int n = num.top();
//            num.pop();
//            for (int i = 0; i < n; i++)
//                ret += tmp;
//            let_bra.pop();
//        }
//    }
//
//    return ret;
//}


string decodeString(string s)
{
    stack<int> num;
    stack<string> str;
    str.push("");
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] >= '0' && s[i] <= '9')
        {
            int k = 0;
            while (i < s.size() && s[i] >= '0' && s[i] <= '9')
                k = k * 10 + (s[i++] - '0');
            num.push(k);
            i--;
        }
        else if (s[i] >= 'a' && s[i] <= 'z')
        {
            string tmp;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.top() += tmp;
        }
        else if (s[i] == '[')
        {
            string tmp;
            i++;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.push(tmp);
        }
        else if (s[i] == ']')
        {
            int n = num.top();
            num.pop();
            string tmp;
            string front = str.top();
            str.pop();
            for (int j = 0; j < n; j++)
                tmp += front;
            str.top() += tmp;
        }
    }

    return str.top();
}


int main()
{
    string s = "3[a]2[bc]";
    string ret = decodeString(s);
    cout << ret << endl;

    return 0;
}



#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


//string multiply(string num1, string num2)
//{
//    string ret;
//    int m = num1.size(), n = num2.size();
//    vector<int> vs(m + n - 1);
//    reverse(num1.begin(), num1.end());
//    reverse(num2.begin(), num2.end());
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//        }
//    }
//
//    int t = 0;
//    for (int i = 0; i < vs.size(); i++)
//    {
//        ret += to_string((vs[i] + t) % 10);
//        t = (vs[i] + t) / 10;
//    }
//    reverse(ret.begin(), ret.end());
//
//    return ret;
//}
//
//int main()
//{
//	string n1 = "123";
//	string n2 = "456";
//
//    string ret = multiply(n1, n2);
//
//		cout << ret << endl;
//		return 0;
//}
//
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        //无进位相乘，然后相加，最后处理进位
//        if (strcmp(num1.c_str(), "0") == 0 || strcmp(num2.c_str(), "0") == 0)  //处理前导0
//            return "0";
//        string ret;
//        int m = num1.size(), n = num2.size();
//        vector<int> vs(m + n - 1);
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//
//        int t = 0;
//        for (int i = 0; i < vs.size(); i++)
//        {
//            ret += to_string((vs[i] + t) % 10);
//            t = (vs[i] + t) / 10;
//        }
//
//        if (t != 0)
//            ret += to_string(t);
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    string addBinary(string a, string b)
//    {
//        int t = 0;
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        while (i >= 0 || j >= 0 || t != 0)
//        {
//            if (i >= 0)
//            {
//                t = t + (a[i] - '0');
//                i--;
//            }
//
//            if (j >= 0)
//            {
//                t = t + (b[j] - '0');
//                j--;
//            }
//
//            ret += to_string(t % 2);
//            t /= 2;
//        }
//
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    string removeDuplicates(string s)
//    {
//
//        string ret;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (ret.size() != 0 && ret.back() == s[i])
//                ret.pop_back();
//            else
//                ret += s[i];
//        }
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string str1, str2;
//        int i = 0, j = 0;
//        while (i < s.size() || j < t.size())
//        {
//            if (i < s.size())
//            {
//                if (s[i] == '#')
//                {
//                    if (str1.size())
//                        str1.pop_back();
//                }
//                else
//                    str1 += s[j];
//            }
//
//            if (j < t.size())
//            {
//                if (t[j] == '#')
//                {
//                    if (str2.size())
//                        str2.pop_back();
//                }
//                else
//                    str2 += t[j];
//            }
//            i++;
//            j++;
//        }
//
//        if (strcmp(str1.c_str(), str2.c_str()) == 0)
//            return true;
//        else
//            return false;
//    }
//};



//string decodeString(string s)
//{
//    stack<int> num;
//    stack<char> let_bra;
//    string ret;
//    int flag = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s[i] >= '0' && s[i] <= '9')
//            num.push(s[i] - '0');
//        else if (s[i] >= 'a' && s[i] <= 'z')
//        {
//            if (flag == 0)
//            {
//                ret += s[i];
//            }
//            else if (flag == 1)
//                let_bra.push(s[i]);
//        }
//        else if (s[i] == '[')
//        {
//            flag = 1;
//            let_bra.push(s[i]);
//        }
//        else if (s[i] == ']')
//        {
//            flag = 0;
//            string tmp;
//            while (let_bra.top() != '[')
//            {
//                tmp += let_bra.top();
//                let_bra.pop();
//            }
//            reverse(tmp.begin(), tmp.end());
//            int n = num.top();
//            num.pop();
//            for (int i = 0; i < n; i++)
//                ret += tmp;
//            let_bra.pop();
//        }
//    }
//
//    return ret;
//}


string decodeString(string s)
{
    stack<int> num;
    stack<string> str;
    str.push("");
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] >= '0' && s[i] <= '9')
        {
            int k = 0;
            while (i < s.size() && s[i] >= '0' && s[i] <= '9')
                k = k * 10 + (s[i++] - '0');
            num.push(k);
            i--;
        }
        else if (s[i] >= 'a' && s[i] <= 'z')
        {
            string tmp;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.top() += tmp;
        }
        else if (s[i] == '[')
        {
            string tmp;
            i++;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.push(tmp);
        }
        else if (s[i] == ']')
        {
            int n = num.top();
            num.pop();
            string tmp;
            string front = str.top();
            str.pop();
            for (int j = 0; j < n; j++)
                tmp += front;
            str.top() += tmp;
        }
    }

    return str.top();
}


int main()
{
    string s = "3[a]2[bc]";
    string ret = decodeString(s);
    cout << ret << endl;

    return 0;
}



#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


//string multiply(string num1, string num2)
//{
//    string ret;
//    int m = num1.size(), n = num2.size();
//    vector<int> vs(m + n - 1);
//    reverse(num1.begin(), num1.end());
//    reverse(num2.begin(), num2.end());
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//        }
//    }
//
//    int t = 0;
//    for (int i = 0; i < vs.size(); i++)
//    {
//        ret += to_string((vs[i] + t) % 10);
//        t = (vs[i] + t) / 10;
//    }
//    reverse(ret.begin(), ret.end());
//
//    return ret;
//}
//
//int main()
//{
//	string n1 = "123";
//	string n2 = "456";
//
//    string ret = multiply(n1, n2);
//
//		cout << ret << endl;
//		return 0;
//}
//
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        //无进位相乘，然后相加，最后处理进位
//        if (strcmp(num1.c_str(), "0") == 0 || strcmp(num2.c_str(), "0") == 0)  //处理前导0
//            return "0";
//        string ret;
//        int m = num1.size(), n = num2.size();
//        vector<int> vs(m + n - 1);
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//
//        int t = 0;
//        for (int i = 0; i < vs.size(); i++)
//        {
//            ret += to_string((vs[i] + t) % 10);
//            t = (vs[i] + t) / 10;
//        }
//
//        if (t != 0)
//            ret += to_string(t);
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    string addBinary(string a, string b)
//    {
//        int t = 0;
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        while (i >= 0 || j >= 0 || t != 0)
//        {
//            if (i >= 0)
//            {
//                t = t + (a[i] - '0');
//                i--;
//            }
//
//            if (j >= 0)
//            {
//                t = t + (b[j] - '0');
//                j--;
//            }
//
//            ret += to_string(t % 2);
//            t /= 2;
//        }
//
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    string removeDuplicates(string s)
//    {
//
//        string ret;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (ret.size() != 0 && ret.back() == s[i])
//                ret.pop_back();
//            else
//                ret += s[i];
//        }
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string str1, str2;
//        int i = 0, j = 0;
//        while (i < s.size() || j < t.size())
//        {
//            if (i < s.size())
//            {
//                if (s[i] == '#')
//                {
//                    if (str1.size())
//                        str1.pop_back();
//                }
//                else
//                    str1 += s[j];
//            }
//
//            if (j < t.size())
//            {
//                if (t[j] == '#')
//                {
//                    if (str2.size())
//                        str2.pop_back();
//                }
//                else
//                    str2 += t[j];
//            }
//            i++;
//            j++;
//        }
//
//        if (strcmp(str1.c_str(), str2.c_str()) == 0)
//            return true;
//        else
//            return false;
//    }
//};



//string decodeString(string s)
//{
//    stack<int> num;
//    stack<char> let_bra;
//    string ret;
//    int flag = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s[i] >= '0' && s[i] <= '9')
//            num.push(s[i] - '0');
//        else if (s[i] >= 'a' && s[i] <= 'z')
//        {
//            if (flag == 0)
//            {
//                ret += s[i];
//            }
//            else if (flag == 1)
//                let_bra.push(s[i]);
//        }
//        else if (s[i] == '[')
//        {
//            flag = 1;
//            let_bra.push(s[i]);
//        }
//        else if (s[i] == ']')
//        {
//            flag = 0;
//            string tmp;
//            while (let_bra.top() != '[')
//            {
//                tmp += let_bra.top();
//                let_bra.pop();
//            }
//            reverse(tmp.begin(), tmp.end());
//            int n = num.top();
//            num.pop();
//            for (int i = 0; i < n; i++)
//                ret += tmp;
//            let_bra.pop();
//        }
//    }
//
//    return ret;
//}


string decodeString(string s)
{
    stack<int> num;
    stack<string> str;
    str.push("");
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] >= '0' && s[i] <= '9')
        {
            int k = 0;
            while (i < s.size() && s[i] >= '0' && s[i] <= '9')
                k = k * 10 + (s[i++] - '0');
            num.push(k);
            i--;
        }
        else if (s[i] >= 'a' && s[i] <= 'z')
        {
            string tmp;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.top() += tmp;
        }
        else if (s[i] == '[')
        {
            string tmp;
            i++;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.push(tmp);
        }
        else if (s[i] == ']')
        {
            int n = num.top();
            num.pop();
            string tmp;
            string front = str.top();
            str.pop();
            for (int j = 0; j < n; j++)
                tmp += front;
            str.top() += tmp;
        }
    }

    return str.top();
}


int main()
{
    string s = "3[a]2[bc]";
    string ret = decodeString(s);
    cout << ret << endl;

    return 0;
}


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


//string multiply(string num1, string num2)
//{
//    string ret;
//    int m = num1.size(), n = num2.size();
//    vector<int> vs(m + n - 1);
//    reverse(num1.begin(), num1.end());
//    reverse(num2.begin(), num2.end());
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//        }
//    }
//
//    int t = 0;
//    for (int i = 0; i < vs.size(); i++)
//    {
//        ret += to_string((vs[i] + t) % 10);
//        t = (vs[i] + t) / 10;
//    }
//    reverse(ret.begin(), ret.end());
//
//    return ret;
//}
//
//int main()
//{
//	string n1 = "123";
//	string n2 = "456";
//
//    string ret = multiply(n1, n2);
//
//		cout << ret << endl;
//		return 0;
//}
//
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        //无进位相乘，然后相加，最后处理进位
//        if (strcmp(num1.c_str(), "0") == 0 || strcmp(num2.c_str(), "0") == 0)  //处理前导0
//            return "0";
//        string ret;
//        int m = num1.size(), n = num2.size();
//        vector<int> vs(m + n - 1);
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//
//        int t = 0;
//        for (int i = 0; i < vs.size(); i++)
//        {
//            ret += to_string((vs[i] + t) % 10);
//            t = (vs[i] + t) / 10;
//        }
//
//        if (t != 0)
//            ret += to_string(t);
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    string addBinary(string a, string b)
//    {
//        int t = 0;
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        while (i >= 0 || j >= 0 || t != 0)
//        {
//            if (i >= 0)
//            {
//                t = t + (a[i] - '0');
//                i--;
//            }
//
//            if (j >= 0)
//            {
//                t = t + (b[j] - '0');
//                j--;
//            }
//
//            ret += to_string(t % 2);
//            t /= 2;
//        }
//
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    string removeDuplicates(string s)
//    {
//
//        string ret;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (ret.size() != 0 && ret.back() == s[i])
//                ret.pop_back();
//            else
//                ret += s[i];
//        }
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string str1, str2;
//        int i = 0, j = 0;
//        while (i < s.size() || j < t.size())
//        {
//            if (i < s.size())
//            {
//                if (s[i] == '#')
//                {
//                    if (str1.size())
//                        str1.pop_back();
//                }
//                else
//                    str1 += s[j];
//            }
//
//            if (j < t.size())
//            {
//                if (t[j] == '#')
//                {
//                    if (str2.size())
//                        str2.pop_back();
//                }
//                else
//                    str2 += t[j];
//            }
//            i++;
//            j++;
//        }
//
//        if (strcmp(str1.c_str(), str2.c_str()) == 0)
//            return true;
//        else
//            return false;
//    }
//};



//string decodeString(string s)
//{
//    stack<int> num;
//    stack<char> let_bra;
//    string ret;
//    int flag = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s[i] >= '0' && s[i] <= '9')
//            num.push(s[i] - '0');
//        else if (s[i] >= 'a' && s[i] <= 'z')
//        {
//            if (flag == 0)
//            {
//                ret += s[i];
//            }
//            else if (flag == 1)
//                let_bra.push(s[i]);
//        }
//        else if (s[i] == '[')
//        {
//            flag = 1;
//            let_bra.push(s[i]);
//        }
//        else if (s[i] == ']')
//        {
//            flag = 0;
//            string tmp;
//            while (let_bra.top() != '[')
//            {
//                tmp += let_bra.top();
//                let_bra.pop();
//            }
//            reverse(tmp.begin(), tmp.end());
//            int n = num.top();
//            num.pop();
//            for (int i = 0; i < n; i++)
//                ret += tmp;
//            let_bra.pop();
//        }
//    }
//
//    return ret;
//}


string decodeString(string s)
{
    stack<int> num;
    stack<string> str;
    str.push("");
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] >= '0' && s[i] <= '9')
        {
            int k = 0;
            while (i < s.size() && s[i] >= '0' && s[i] <= '9')
                k = k * 10 + (s[i++] - '0');
            num.push(k);
            i--;
        }
        else if (s[i] >= 'a' && s[i] <= 'z')
        {
            string tmp;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.top() += tmp;
        }
        else if (s[i] == '[')
        {
            string tmp;
            i++;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.push(tmp);
        }
        else if (s[i] == ']')
        {
            int n = num.top();
            num.pop();
            string tmp;
            string front = str.top();
            str.pop();
            for (int j = 0; j < n; j++)
                tmp += front;
            str.top() += tmp;
        }
    }

    return str.top();
}


int main()
{
    string s = "3[a]2[bc]";
    string ret = decodeString(s);
    cout << ret << endl;

    return 0;
}


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


//string multiply(string num1, string num2)
//{
//    string ret;
//    int m = num1.size(), n = num2.size();
//    vector<int> vs(m + n - 1);
//    reverse(num1.begin(), num1.end());
//    reverse(num2.begin(), num2.end());
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//        }
//    }
//
//    int t = 0;
//    for (int i = 0; i < vs.size(); i++)
//    {
//        ret += to_string((vs[i] + t) % 10);
//        t = (vs[i] + t) / 10;
//    }
//    reverse(ret.begin(), ret.end());
//
//    return ret;
//}
//
//int main()
//{
//	string n1 = "123";
//	string n2 = "456";
//
//    string ret = multiply(n1, n2);
//
//		cout << ret << endl;
//		return 0;
//}
//
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        //无进位相乘，然后相加，最后处理进位
//        if (strcmp(num1.c_str(), "0") == 0 || strcmp(num2.c_str(), "0") == 0)  //处理前导0
//            return "0";
//        string ret;
//        int m = num1.size(), n = num2.size();
//        vector<int> vs(m + n - 1);
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//
//        int t = 0;
//        for (int i = 0; i < vs.size(); i++)
//        {
//            ret += to_string((vs[i] + t) % 10);
//            t = (vs[i] + t) / 10;
//        }
//
//        if (t != 0)
//            ret += to_string(t);
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    string addBinary(string a, string b)
//    {
//        int t = 0;
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        while (i >= 0 || j >= 0 || t != 0)
//        {
//            if (i >= 0)
//            {
//                t = t + (a[i] - '0');
//                i--;
//            }
//
//            if (j >= 0)
//            {
//                t = t + (b[j] - '0');
//                j--;
//            }
//
//            ret += to_string(t % 2);
//            t /= 2;
//        }
//
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    string removeDuplicates(string s)
//    {
//
//        string ret;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (ret.size() != 0 && ret.back() == s[i])
//                ret.pop_back();
//            else
//                ret += s[i];
//        }
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string str1, str2;
//        int i = 0, j = 0;
//        while (i < s.size() || j < t.size())
//        {
//            if (i < s.size())
//            {
//                if (s[i] == '#')
//                {
//                    if (str1.size())
//                        str1.pop_back();
//                }
//                else
//                    str1 += s[j];
//            }
//
//            if (j < t.size())
//            {
//                if (t[j] == '#')
//                {
//                    if (str2.size())
//                        str2.pop_back();
//                }
//                else
//                    str2 += t[j];
//            }
//            i++;
//            j++;
//        }
//
//        if (strcmp(str1.c_str(), str2.c_str()) == 0)
//            return true;
//        else
//            return false;
//    }
//};



//string decodeString(string s)
//{
//    stack<int> num;
//    stack<char> let_bra;
//    string ret;
//    int flag = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s[i] >= '0' && s[i] <= '9')
//            num.push(s[i] - '0');
//        else if (s[i] >= 'a' && s[i] <= 'z')
//        {
//            if (flag == 0)
//            {
//                ret += s[i];
//            }
//            else if (flag == 1)
//                let_bra.push(s[i]);
//        }
//        else if (s[i] == '[')
//        {
//            flag = 1;
//            let_bra.push(s[i]);
//        }
//        else if (s[i] == ']')
//        {
//            flag = 0;
//            string tmp;
//            while (let_bra.top() != '[')
//            {
//                tmp += let_bra.top();
//                let_bra.pop();
//            }
//            reverse(tmp.begin(), tmp.end());
//            int n = num.top();
//            num.pop();
//            for (int i = 0; i < n; i++)
//                ret += tmp;
//            let_bra.pop();
//        }
//    }
//
//    return ret;
//}


