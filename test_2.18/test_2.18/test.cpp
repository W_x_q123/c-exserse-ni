#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <cctype>
#include <mutex>
#include "util.hpp"
#include "boost/algorithm/string/case_conv.hpp"
#include "log.hpp"

namespace ns_index
{
    class DocInfo
    {
    public:
        std::string title;   // 解析好的文档的标题
        std::string content; // 解析好的文档的内容
        std::string url;     // 解析好的文档的url
        uint64_t doc_id;
    };

    class InvertedElem
    {
    public:
        uint64_t doc_id;  // 文档id
        std::string word; // 关键词
        int weight;       // 权重
    };

    typedef std::vector<InvertedElem> InvertedList;
    class index
    {
    private:
        // 正排索引用数组，直接通过文档id找到对应文档，数组下标就是天然的文档id
        std::vector<DocInfo> forward_index;
        // 倒排索引是一个关键字和一组（个）InvertedElem 对应[关键字和倒排拉链的映射关系]
        std::unordered_map<std::string, InvertedList> inverted_index;

    private:
        index() {}
        ~index() {}
        index(const index&) = delete;
        index& operator=(const index&) = delete;
        static index* instance;
        static std::mutex mtx;

    public:
        static index* GetInstance()
        {
            if (instance == nullptr)
            {
                mtx.lock();
                if (instance == nullptr)
                {
                    instance = new index();
                }
                mtx.unlock();
            }

            return instance;
        }
        // 1.获取正排索引
        DocInfo* GetForwardIndex(uint64_t doc_id)
        {
            if (doc_id >= forward_index.size())
            {
                // std::cerr << doc_id << " out range! error!" << std::endl;
                LOG(FATAL, std::to_string(doc_id) + " out range! error!");
                return nullptr;
            }

            return &forward_index[doc_id];
        }

        // 2.获取倒排索引,根据关键字string，获取倒排拉链
        InvertedList* GetInvertedList(const std::string& word)
        {
            auto iter = inverted_index.find(word);
            if (iter == inverted_index.end())
            {
                // std::cerr << word << " have no invertedlist" << std::endl;
                LOG(FATAL, word + " have no invertedlist");
                return nullptr;
            }
            return &iter->second;
        }

        // 3.构建索引
        // input: data/raw_html/raw.txt
        bool BuildIndex(const std::string& input) // parser 清洗好的数据交给我
        {
            std::ifstream in(input, std::ios::in | std::ios::binary);
            if (!in.is_open())
            {
                // std::cerr << "open " << input << " failed" << std::endl;
                LOG(FATAL, "open " + input + " failed");
                return false;
            }
            std::string line;
            int cnt = 0;
            while (std::getline(in, line))
            {
                // 构建正排索引
                DocInfo* doc = BulidForwardIndex(line);
                if (doc == nullptr)
                {
                    // std::cerr << "debug: " << line << " error " << std::endl;
                    LOG(DEBUG, "debug: " + line + " error");
                    continue;
                }
                // 构建倒排索引
                BuildInvertedIndex(*doc);
                cnt++;
                if (cnt % 50 == 0)
                {
                    // std::cout << "已建立索引: " << cnt << std::endl;
                    LOG(NORMAL, "已建立索引:" + std::to_string(cnt));
                }
            }
            return true;
        }

    private:
        DocInfo* BulidForwardIndex(const std::string& line)
        {
            // 1.解析line，切割字符串
            // line-> 3 string title content url -> vector<string>
            std::vector<std::string> results;
            const std::string sep = "\3";
            ns_util::StringUtil::Split(line, &results, sep);
            if (results.size() != 3)
            {
                return nullptr;
            }

            DocInfo doc;
            doc.title = results[0];
            doc.content = results[1];
            doc.url = results[2];
            doc.doc_id = forward_index.size();
            forward_index.push_back(std::move(doc));

            return &forward_index.back();
        }

        bool BuildInvertedIndex(const DocInfo& doc)
        {
            // DocInfo:title content url doc_id
            // word->倒排拉链 incertedList

            struct word_cnt
            {
                int title_cnt;
                int content_cnt;
                word_cnt() : title_cnt(0), content_cnt(0) {}
            };

            std::unordered_map<std::string, word_cnt> word_map; // 用来暂存词频的映射表

            // 对title进行分词
            std::vector<std::string> title_word;
            ns_util::JiebaUtil::CutString(doc.title, &title_word);

            // 对title进行词频统计
            for (const auto& s : title_word)
            {
                // boost::to_lower(s); // 统一转成小写
                for (const auto& ch : s)
                    tolower(ch);
                word_map[s].title_cnt++;
            }

            // 对content进行分词
            std::vector<std::string> content_word;
            ns_util::JiebaUtil::CutString(doc.content, &content_word);

            // 对content进行词频统计
            for (const auto& s : content_word)
            {
                // boost::to_lower(s);
                for (const auto& ch : s)
                    tolower(ch);
                word_map[s].content_cnt++;
            }

#define X 10
#define Y 1
            // 构建倒排
            for (const auto& s : word_map)
            {
                InvertedElem elem;
                elem.doc_id = doc.doc_id;
                elem.word = s.first;
                elem.weight = X * s.second.title_cnt + Y * s.second.content_cnt;
                InvertedList& inverted_list = inverted_index[s.first];
                inverted_list.push_back(std::move(elem));
            }
            return true;
        }
    };

    index* index::instance = nullptr;
    std::mutex index::mtx;
};