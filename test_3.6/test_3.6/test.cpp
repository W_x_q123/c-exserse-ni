#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <queue>


using namespace std;



struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

//int widthOfBinaryTree(TreeNode* root)
//{
//    queue<pair<TreeNode, unsigned int>> q;
//    q.push(make_pair( root,1 ));
//    int ret = 0;
//    while (!q.size())
//    {
//        int sz = q.size();
//        for (int i = 0; i < sz; i++)
//        {
//            auto t = q.front();
//            q.pop();
//            if ((t.first)->left) q.push({ (t.first)->left }, 2 * (t.second));
//            if ((t.first)->right) q.push({ (t.right)->left }, 2 * (t.second) + 1);
//        }
//
//        ret = q.front().second - q.back().second + 1;
//    }
//
//    return ret;
//}


int widthOfBinaryTree(TreeNode* root)
{
    vector<pair<TreeNode*, unsigned int>> q;
    q.push_back({ root,1 });
    unsigned int ret = 0;
    while (q.size())
    {
        auto& t1 = q[0];  
        auto& t2 = q.back();
        ret = max(ret, y2 - y1 + 1);

        vector<pair<TreeNode*, unsigned int>> tmp;
        for (auto& [x, y] : q)
        {
            if (x->left) tmp.push_back({ x->left,2 * y });
            if (x->right) tmp.push_back({ x->right,2 * y + 1 });
        }
        q = tmp;
    }

    return ret;
}

int main()
{


    return 0;

}



/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root)
    {
        vector<pair<TreeNode*, unsigned int>> q;
        q.push_back({ root,1 });
        unsigned int ret = 0;
        while (q.size())
        {
            auto& [x1, y1] = q[0];
            auto& [x2, y2] = q.back();
            ret = max(ret, y2 - y1 + 1);

            vector<pair<TreeNode*, unsigned int>> tmp;
            for (auto& [x, y] : q)
            {
                if (x->left) tmp.push_back({ x->left,2 * y });
                if (x->right) tmp.push_back({ x->right,2 * y + 1 });
            }
            q = tmp;
        }

        return ret;
    }
};


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> largestValues(TreeNode* root)
    {
        vector<int> ret;
        if (nullptr == root)
            return ret;
        vector<TreeNode*> q;  //数组模拟队列
        q.push_back(root);
        int res = INT_MIN;
        while (q.size())
        {
            int sz = q.size();
            for (int i = 0; i < sz; i++)
                res = max(res, q[i]->val);
            ret.push_back(res);
            res = INT_MIN;
            vector<TreeNode*> tmp;
            for (int i = 0; i < sz; i++)
            {
                if (q[i]->left) tmp.push_back(q[i]->left);
                if (q[i]->right) tmp.push_back(q[i]->right);
            }

            q = tmp;
        }
        return ret;
    }
};