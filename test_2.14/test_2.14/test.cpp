#pragma once

#include "Index.hpp"
#include "util.hpp"
#include <algorithm>
#include "jsoncpp-master/include/json/json.h"
#include "boost/algorithm/string/case_conv.hpp"
#include <cctype>

namespace ns_searcher
{
    struct InvertedElemPrint
    {
        uint64_t doc_id;
        int weight;
        std::vector<std::string> words;
        InvertedElemPrint() : doc_id(0), weight(0) {}
    };
    class searcher
    {
    private:
        ns_index::index* index; // 供系统进行查找的索引
    public:
        searcher() {}
        ~searcher() {}

    public:
        void InitSearcher(const std::string& input)
        {
            // 1.获取或者创建index对象
            index = ns_index::index::GetInstance();
            std::cout << "获取index对象完成" << std::endl;
            // 2.根据index对象建立索引
            index->BuildIndex(input);
            std::cout << "索引建立成功" << std::endl;
        }

        void Search(const std::string& query, std::string* json_string)
        {
            // 1.[分词]:对我们的query进行按照searcher的要求分词
            std::vector<std::string> words;
            ns_util::JiebaUtil::CutString(query, &words);
            // std::cout << "分词结果:" << std::endl;
            // for (const auto &s : words)
            // {
            //     std::cout << s << " ";
            // }
            // std::cout << std::endl;
            // 2.[触发]:根据分词的各个‘词’，进行index查找
            // ns_index::InvertedList inverted_list_all;
            std::unordered_map<uint64_t, InvertedElemPrint> tokens_map; // 用来去重
            std::vector<InvertedElemPrint> inverted_list_all;           // 用来存放去重后的倒排拉链
            for (std::string word : words)
            {
                // boost::to_lower(word);
                for (const auto& s : word)
                    tolower(s);
                ns_index::InvertedList* inverted_list = index->GetInvertedList(word);
                if (inverted_list == nullptr)
                    continue;
                // 为倒排拉链去重
                for (const auto& elem : *inverted_list)
                {
                    auto& item = tokens_map[elem.doc_id];
                    item.doc_id = elem.doc_id;
                    item.weight += elem.weight;
                    item.words.push_back(elem.word);
                }

                // inverted_list_all.insert(inverted_list_all.end(), inverted_list->begin(), inverted_list->end());
            }

            // 将去重后的倒排拉链汇总
            for (const auto& item : tokens_map)
            {
                inverted_list_all.push_back(item.second);
            }
            // 3.[合并排序]:汇总查找结果，按照(相关性weight)降序排序
            // std::sort(inverted_list_all.begin(), inverted_list_all.end(),
            //           [](const ns_index::InvertedElem &e1, const ns_index::InvertedElem &e2)
            //           { return e1.weight > e2.weight; });
            std::sort(inverted_list_all.begin(), inverted_list_all.end(), [](const InvertedElemPrint& e1, const InvertedElemPrint& e2)
                { return e1.weight > e2.weight; });
            // 4.[构建]:根据查找出来的结果，构建json串
            Json::Value root;
            for (const auto& item : inverted_list_all)
            {
                ns_index::DocInfo* doc = index->GetForwardIndex(item.doc_id);
                Json::Value elem;
                elem["title"] = doc->title;
                elem["desc"] = Getdesc(doc->content, item.words[0]);
                elem["url"] = doc->url;
                // elem["id"] = doc->doc_id;
                // elem["weight"] = item.weight;

                root.append(elem);
            }
            // Json::StyledWriter writer;
            Json::FastWriter writer;
            *json_string = writer.write(root);
        }

        std::string Getdesc(const std::string& content_html, const std::string& word)
        {
            // int pos = content_html.find(word);
            // if (pos == std::string::npos)
            //     return "None1";

            auto iter = std::search(content_html.begin(), content_html.end(), word.begin(), word.end(), [](int x, int y)
                { return tolower(x) == tolower(y); });

            if (iter == content_html.end())
                return "None1";

            int pos = std::distance(content_html.begin(), iter);

            int prev_step = 50;
            int next_step = 100;

            int start = 0;
            int end = content_html.size() - 1;

            if (pos - prev_step > start)
                start = pos - prev_step;
            if (pos + next_step < end)
                end = pos + next_step;

            if (start >= end)
                return "None2";
            std::string desc = content_html.substr(start, end - start) + "...查看更多";
            return desc;
        }
    };
}