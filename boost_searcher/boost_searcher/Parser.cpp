#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>

const std::string src_path = "data/input";          // 要读取的文件的源路径
const std::string output = "data/raw_html/raw.txt"; // 解析文件完成后，放到这个文件

typedef struct DocInfo
{
    std::string title;   // 文档标题
    std::string content; // 文档内容
    std::string url;     // 该文档在官网中的url
} DocInfo_t;

// const & :输入
//* : 输出
//& :输入输出

bool EnumFile(const std::string& src_path, std::vector<std::string>* files_list);
bool ParseHtml(const std::vector<std::string>& files_list, std::vector<DocInfo_t>* results);
bool SaveHtml(const std::vector<DocInfo_t>& results, const std::string& output);

int main()
{
    // 第一步，递归式地把每个html文件名带路径，保存到files_list中，方便后期进行一个一个文件的读取
    std::vector<std::string> files_list;
    if (!EnumFile(src_path, &files_list))
    {
        std::cerr << "enum file error" << std::endl;
        return -1;
    }

    // 第二步，按照files_list读取每个文件的内容，并解析
    std::vector<DocInfo_t> results;
    if (!ParseHtml(files_list, &results)) // 清洗html
    {
        std::cerr << "parsehtml error" << std::endl;
        return 2;
    }

    // 第三步，读取results中的内容，把解析完毕的各个文件的内容，写到output中，按照\3作为每个文档的分隔符
    if (!SaveHtml(results, output)) // 保存清洗好的html
    {
        std::cerr << "savehtml error" << std::endl;
        return 3;
    }

    return 0;
}

bool EnumFile(const std::string& src_path, std::vector<std::string>* files_list)
{
    namespace fs = boost::filesystem;
    fs::path root_path(src_path);

    // 判断路径是否存在
    if (!fs::exists(root_path))
    {
        std::cerr << root_path << " not exist" << std::endl;
        return false;
    }

    // 定义一个空的迭代器，用来进行判断递归结束
    fs::recursive_directory_iterator end;
    for (fs::recursive_directory_iterator iter; iter != end; iter++)
    {
        // 判断是否是普通文件
        if (!fs::is_regular_file(*iter))
            continue;
        // 判断是否是以.html结尾的文件
        if (iter->path().extension() != ".html")
            continue;

        std::cout << "debug: " << iter->path().string() << std::endl;

        files_list->push_back(iter->path().string());
    }
    return true;
}

bool ParseHtml(const std::vector<std::string>& files_list, std::vector<DocInfo_t>* results)
{
    return true;
}

bool SaveHtml(const std::vector<DocInfo_t>& results, std::string* output)
{
    return true;
}