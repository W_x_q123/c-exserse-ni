#include <string>
#include <vector>
#include <iostream>
#include <unordered_set>
#include <unordered_map>

#include <queue>
#include <algorithm>


using namespace std;


//int main()
//{
//	string str = "hello world";
//	string a = "a";
//	char ca = 'a';
//	str.replace(0, 1, 1,ca);
//	cout << str << endl;
//
//	return 0;
//}



//char ds[4] = { 'A','C','G','T' };
//int minMutation(string startGene, string endGene, vector<string>& bank)
//{
//    unordered_set<string> bank_hash;
//    for (auto& str : bank)
//        bank_hash.insert(str);
//    queue<string> q;
//    q.push(startGene);
//    int ret = 0;
//    while (q.size())
//    {
//        ret++;
//        int sz = q.size();
//        for (int i = 0; i < sz; i++)
//        {
//            auto s = q.front();
//            q.pop();
//            for (int j = 0; j < s.size(); j++)
//            {
//                for (int k = 0; k < 4; k++)
//                {
//                    string tmp = s;
//                    tmp.replace(j, 1, 1, ds[k]);
//                    if (bank_hash.count(tmp))
//                        q.push(tmp);
//                    if (bank_hash.count(tmp) && tmp == endGene)
//                        return ret;
//
//                }
//            }
//        }
//    }
//    return -1;
//}
//
//
//int main()
//{
//    string start = "AACCTTGG";
//    string end = "AATTCCGG";
//    vector<string> bank = { "AATTCCGG","AACCTGGG","AACCCCGG","AACCTACC" };
//
//    cout << minMutation(start, end, bank) << endl;
//
//    return 0;
//}



struct cmp
{
	bool operator()(const pair<int, int>& e1, const pair<int, int>& e2)
	{
		return e1.first < e2.first;
	}
};


int main()
{

	unordered_map<int, int> m;
	m.insert({ 2,1 });
	m.insert({ 5,1 });
	m.insert({ 1,1 });
	m.insert({ 3,1 });
	m.insert({0,1 });

	sort(m.begin(), m.end(),cmp);
	for (auto e : m)
	{
		cout << e.first << endl;
	}

	cout << endl;

	return 0;


}


#include <string>
#include <vector>
#include <iostream>
#include <unordered_set>
#include <unordered_map>

#include <queue>
#include <algorithm>


using namespace std;


//int main()
//{
//	string str = "hello world";
//	string a = "a";
//	char ca = 'a';
//	str.replace(0, 1, 1,ca);
//	cout << str << endl;
//
//	return 0;
//}



//char ds[4] = { 'A','C','G','T' };
//int minMutation(string startGene, string endGene, vector<string>& bank)
//{
//    unordered_set<string> bank_hash;
//    for (auto& str : bank)
//        bank_hash.insert(str);
//    queue<string> q;
//    q.push(startGene);
//    int ret = 0;
//    while (q.size())
//    {
//        ret++;
//        int sz = q.size();
//        for (int i = 0; i < sz; i++)
//        {
//            auto s = q.front();
//            q.pop();
//            for (int j = 0; j < s.size(); j++)
//            {
//                for (int k = 0; k < 4; k++)
//                {
//                    string tmp = s;
//                    tmp.replace(j, 1, 1, ds[k]);
//                    if (bank_hash.count(tmp))
//                        q.push(tmp);
//                    if (bank_hash.count(tmp) && tmp == endGene)
//                        return ret;
//
//                }
//            }
//        }
//    }
//    return -1;
//}
//
//
//int main()
//{
//    string start = "AACCTTGG";
//    string end = "AATTCCGG";
//    vector<string> bank = { "AATTCCGG","AACCTGGG","AACCCCGG","AACCTACC" };
//
//    cout << minMutation(start, end, bank) << endl;
//
//    return 0;
//}



struct cmp
{
	bool operator()(const pair<int, int>& e1, const pair<int, int>& e2)
	{
		return e1.first < e2.first;
	}
};


int main()
{

	unordered_map<int, int> m;
	m.insert({ 2,1 });
	m.insert({ 5,1 });
	m.insert({ 1,1 });
	m.insert({ 3,1 });
	m.insert({ 0,1 });

	sort(m.begin(), m.end(), cmp);
	for (auto e : m)
	{
		cout << e.first << endl;
	}

	cout << endl;

	return 0;


}

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


//string multiply(string num1, string num2)
//{
//    string ret;
//    int m = num1.size(), n = num2.size();
//    vector<int> vs(m + n - 1);
//    reverse(num1.begin(), num1.end());
//    reverse(num2.begin(), num2.end());
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//        }
//    }
//
//    int t = 0;
//    for (int i = 0; i < vs.size(); i++)
//    {
//        ret += to_string((vs[i] + t) % 10);
//        t = (vs[i] + t) / 10;
//    }
//    reverse(ret.begin(), ret.end());
//
//    return ret;
//}
//
//int main()
//{
//	string n1 = "123";
//	string n2 = "456";
//
//    string ret = multiply(n1, n2);
//
//		cout << ret << endl;
//		return 0;
//}
//
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        //无进位相乘，然后相加，最后处理进位
//        if (strcmp(num1.c_str(), "0") == 0 || strcmp(num2.c_str(), "0") == 0)  //处理前导0
//            return "0";
//        string ret;
//        int m = num1.size(), n = num2.size();
//        vector<int> vs(m + n - 1);
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//
//        int t = 0;
//        for (int i = 0; i < vs.size(); i++)
//        {
//            ret += to_string((vs[i] + t) % 10);
//            t = (vs[i] + t) / 10;
//        }
//
//        if (t != 0)
//            ret += to_string(t);
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    string addBinary(string a, string b)
//    {
//        int t = 0;
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        while (i >= 0 || j >= 0 || t != 0)
//        {
//            if (i >= 0)
//            {
//                t = t + (a[i] - '0');
//                i--;
//            }
//
//            if (j >= 0)
//            {
//                t = t + (b[j] - '0');
//                j--;
//            }
//
//            ret += to_string(t % 2);
//            t /= 2;
//        }
//
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    string removeDuplicates(string s)
//    {
//
//        string ret;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (ret.size() != 0 && ret.back() == s[i])
//                ret.pop_back();
//            else
//                ret += s[i];
//        }
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string str1, str2;
//        int i = 0, j = 0;
//        while (i < s.size() || j < t.size())
//        {
//            if (i < s.size())
//            {
//                if (s[i] == '#')
//                {
//                    if (str1.size())
//                        str1.pop_back();
//                }
//                else
//                    str1 += s[j];
//            }
//
//            if (j < t.size())
//            {
//                if (t[j] == '#')
//                {
//                    if (str2.size())
//                        str2.pop_back();
//                }
//                else
//                    str2 += t[j];
//            }
//            i++;
//            j++;
//        }
//
//        if (strcmp(str1.c_str(), str2.c_str()) == 0)
//            return true;
//        else
//            return false;
//    }
//};



//string decodeString(string s)
//{
//    stack<int> num;
//    stack<char> let_bra;
//    string ret;
//    int flag = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s[i] >= '0' && s[i] <= '9')
//            num.push(s[i] - '0');
//        else if (s[i] >= 'a' && s[i] <= 'z')
//        {
//            if (flag == 0)
//            {
//                ret += s[i];
//            }
//            else if (flag == 1)
//                let_bra.push(s[i]);
//        }
//        else if (s[i] == '[')
//        {
//            flag = 1;
//            let_bra.push(s[i]);
//        }
//        else if (s[i] == ']')
//        {
//            flag = 0;
//            string tmp;
//            while (let_bra.top() != '[')
//            {
//                tmp += let_bra.top();
//                let_bra.pop();
//            }
//            reverse(tmp.begin(), tmp.end());
//            int n = num.top();
//            num.pop();
//            for (int i = 0; i < n; i++)
//                ret += tmp;
//            let_bra.pop();
//        }
//    }
//
//    return ret;
//}


string decodeString(string s)
{
    stack<int> num;
    stack<string> str;
    str.push("");
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] >= '0' && s[i] <= '9')
        {
            int k = 0;
            while (i < s.size() && s[i] >= '0' && s[i] <= '9')
                k = k * 10 + (s[i++] - '0');
            num.push(k);
            i--;
        }
        else if (s[i] >= 'a' && s[i] <= 'z')
        {
            string tmp;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.top() += tmp;
        }
        else if (s[i] == '[')
        {
            string tmp;
            i++;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.push(tmp);
        }
        else if (s[i] == ']')
        {
            int n = num.top();
            num.pop();
            string tmp;
            string front = str.top();
            str.pop();
            for (int j = 0; j < n; j++)
                tmp += front;
            str.top() += tmp;
        }
    }

    return str.top();
}


int main()
{
    string s = "3[a]2[bc]";
    string ret = decodeString(s);
    cout << ret << endl;

    return 0;
}


#include <string>
#include <vector>
#include <iostream>
#include <unordered_set>
#include <unordered_map>

#include <queue>
#include <algorithm>


using namespace std;


//int main()
//{
//	string str = "hello world";
//	string a = "a";
//	char ca = 'a';
//	str.replace(0, 1, 1,ca);
//	cout << str << endl;
//
//	return 0;
//}



//char ds[4] = { 'A','C','G','T' };
//int minMutation(string startGene, string endGene, vector<string>& bank)
//{
//    unordered_set<string> bank_hash;
//    for (auto& str : bank)
//        bank_hash.insert(str);
//    queue<string> q;
//    q.push(startGene);
//    int ret = 0;
//    while (q.size())
//    {
//        ret++;
//        int sz = q.size();
//        for (int i = 0; i < sz; i++)
//        {
//            auto s = q.front();
//            q.pop();
//            for (int j = 0; j < s.size(); j++)
//            {
//                for (int k = 0; k < 4; k++)
//                {
//                    string tmp = s;
//                    tmp.replace(j, 1, 1, ds[k]);
//                    if (bank_hash.count(tmp))
//                        q.push(tmp);
//                    if (bank_hash.count(tmp) && tmp == endGene)
//                        return ret;
//
//                }
//            }
//        }
//    }
//    return -1;
//}
//
//
//int main()
//{
//    string start = "AACCTTGG";
//    string end = "AATTCCGG";
//    vector<string> bank = { "AATTCCGG","AACCTGGG","AACCCCGG","AACCTACC" };
//
//    cout << minMutation(start, end, bank) << endl;
//
//    return 0;
//}



struct cmp
{
    bool operator()(const pair<int, int>& e1, const pair<int, int>& e2)
    {
        return e1.first < e2.first;
    }
};


int main()
{

    unordered_map<int, int> m;
    m.insert({ 2,1 });
    m.insert({ 5,1 });
    m.insert({ 1,1 });
    m.insert({ 3,1 });
    m.insert({ 0,1 });

    sort(m.begin(), m.end(), cmp);
    for (auto e : m)
    {
        cout << e.first << endl;
    }

    cout << endl;

    return 0;


}


#include <string>
#include <vector>
#include <iostream>
#include <unordered_set>
#include <unordered_map>

#include <queue>
#include <algorithm>


using namespace std;


//int main()
//{
//	string str = "hello world";
//	string a = "a";
//	char ca = 'a';
//	str.replace(0, 1, 1,ca);
//	cout << str << endl;
//
//	return 0;
//}



//char ds[4] = { 'A','C','G','T' };
//int minMutation(string startGene, string endGene, vector<string>& bank)
//{
//    unordered_set<string> bank_hash;
//    for (auto& str : bank)
//        bank_hash.insert(str);
//    queue<string> q;
//    q.push(startGene);
//    int ret = 0;
//    while (q.size())
//    {
//        ret++;
//        int sz = q.size();
//        for (int i = 0; i < sz; i++)
//        {
//            auto s = q.front();
//            q.pop();
//            for (int j = 0; j < s.size(); j++)
//            {
//                for (int k = 0; k < 4; k++)
//                {
//                    string tmp = s;
//                    tmp.replace(j, 1, 1, ds[k]);
//                    if (bank_hash.count(tmp))
//                        q.push(tmp);
//                    if (bank_hash.count(tmp) && tmp == endGene)
//                        return ret;
//
//                }
//            }
//        }
//    }
//    return -1;
//}
//
//
//int main()
//{
//    string start = "AACCTTGG";
//    string end = "AATTCCGG";
//    vector<string> bank = { "AATTCCGG","AACCTGGG","AACCCCGG","AACCTACC" };
//
//    cout << minMutation(start, end, bank) << endl;
//
//    return 0;
//}



struct cmp
{
    bool operator()(const pair<int, int>& e1, const pair<int, int>& e2)
    {
        return e1.first < e2.first;
    }
};


int main()
{

    unordered_map<int, int> m;
    m.insert({ 2,1 });
    m.insert({ 5,1 });
    m.insert({ 1,1 });
    m.insert({ 3,1 });
    m.insert({ 0,1 });

    sort(m.begin(), m.end(), cmp);
    for (auto e : m)
    {
        cout << e.first << endl;
    }

    cout << endl;

    return 0;


}

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


//string multiply(string num1, string num2)
//{
//    string ret;
//    int m = num1.size(), n = num2.size();
//    vector<int> vs(m + n - 1);
//    reverse(num1.begin(), num1.end());
//    reverse(num2.begin(), num2.end());
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//        }
//    }
//
//    int t = 0;
//    for (int i = 0; i < vs.size(); i++)
//    {
//        ret += to_string((vs[i] + t) % 10);
//        t = (vs[i] + t) / 10;
//    }
//    reverse(ret.begin(), ret.end());
//
//    return ret;
//}
//
//int main()
//{
//	string n1 = "123";
//	string n2 = "456";
//
//    string ret = multiply(n1, n2);
//
//		cout << ret << endl;
//		return 0;
//}
//
//
//
//class Solution {
//public:
//    string multiply(string num1, string num2)
//    {
//        //无进位相乘，然后相加，最后处理进位
//        if (strcmp(num1.c_str(), "0") == 0 || strcmp(num2.c_str(), "0") == 0)  //处理前导0
//            return "0";
//        string ret;
//        int m = num1.size(), n = num2.size();
//        vector<int> vs(m + n - 1);
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                vs[i + j] = vs[i + j] + (num1[i] - '0') * (num2[j] - '0');
//            }
//        }
//
//        int t = 0;
//        for (int i = 0; i < vs.size(); i++)
//        {
//            ret += to_string((vs[i] + t) % 10);
//            t = (vs[i] + t) / 10;
//        }
//
//        if (t != 0)
//            ret += to_string(t);
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    string addBinary(string a, string b)
//    {
//        int t = 0;
//        string ret;
//        int i = a.size() - 1, j = b.size() - 1;
//        while (i >= 0 || j >= 0 || t != 0)
//        {
//            if (i >= 0)
//            {
//                t = t + (a[i] - '0');
//                i--;
//            }
//
//            if (j >= 0)
//            {
//                t = t + (b[j] - '0');
//                j--;
//            }
//
//            ret += to_string(t % 2);
//            t /= 2;
//        }
//
//        reverse(ret.begin(), ret.end());
//
//        return ret;
//    }
//};
//
//
//class Solution {
//public:
//    string removeDuplicates(string s)
//    {
//
//        string ret;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (ret.size() != 0 && ret.back() == s[i])
//                ret.pop_back();
//            else
//                ret += s[i];
//        }
//
//        return ret;
//    }
//
//};
//
//
//
//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string str1, str2;
//        int i = 0, j = 0;
//        while (i < s.size() || j < t.size())
//        {
//            if (i < s.size())
//            {
//                if (s[i] == '#')
//                {
//                    if (str1.size())
//                        str1.pop_back();
//                }
//                else
//                    str1 += s[j];
//            }
//
//            if (j < t.size())
//            {
//                if (t[j] == '#')
//                {
//                    if (str2.size())
//                        str2.pop_back();
//                }
//                else
//                    str2 += t[j];
//            }
//            i++;
//            j++;
//        }
//
//        if (strcmp(str1.c_str(), str2.c_str()) == 0)
//            return true;
//        else
//            return false;
//    }
//};



//string decodeString(string s)
//{
//    stack<int> num;
//    stack<char> let_bra;
//    string ret;
//    int flag = 0;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (s[i] >= '0' && s[i] <= '9')
//            num.push(s[i] - '0');
//        else if (s[i] >= 'a' && s[i] <= 'z')
//        {
//            if (flag == 0)
//            {
//                ret += s[i];
//            }
//            else if (flag == 1)
//                let_bra.push(s[i]);
//        }
//        else if (s[i] == '[')
//        {
//            flag = 1;
//            let_bra.push(s[i]);
//        }
//        else if (s[i] == ']')
//        {
//            flag = 0;
//            string tmp;
//            while (let_bra.top() != '[')
//            {
//                tmp += let_bra.top();
//                let_bra.pop();
//            }
//            reverse(tmp.begin(), tmp.end());
//            int n = num.top();
//            num.pop();
//            for (int i = 0; i < n; i++)
//                ret += tmp;
//            let_bra.pop();
//        }
//    }
//
//    return ret;
//}


string decodeString(string s)
{
    stack<int> num;
    stack<string> str;
    str.push("");
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] >= '0' && s[i] <= '9')
        {
            int k = 0;
            while (i < s.size() && s[i] >= '0' && s[i] <= '9')
                k = k * 10 + (s[i++] - '0');
            num.push(k);
            i--;
        }
        else if (s[i] >= 'a' && s[i] <= 'z')
        {
            string tmp;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.top() += tmp;
        }
        else if (s[i] == '[')
        {
            string tmp;
            i++;
            while (i < s.size() && s[i] >= 'a' && s[i] <= 'z')
                tmp += s[i++];
            i--;
            str.push(tmp);
        }
        else if (s[i] == ']')
        {
            int n = num.top();
            num.pop();
            string tmp;
            string front = str.top();
            str.pop();
            for (int j = 0; j < n; j++)
                tmp += front;
            str.top() += tmp;
        }
    }

    return str.top();
}


int main()
{
    string s = "3[a]2[bc]";
    string ret = decodeString(s);
    cout << ret << endl;

    return 0;
}