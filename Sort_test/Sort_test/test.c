#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>



//插入排序
void InsertSort(int* arr, int n)
{
	int end = 0, tmp = 0;
	int i = 0;
	for (i = 1; i < n; i++)
	{
		end = i - 1;
		tmp = arr[i];
		while (end >= 0)
		{
			if (tmp < arr[end])
			{
				arr[end + 1] = arr[end];
				end--;
			}
			else
				break;
		}
		arr[end + 1] = tmp;
	}


}


//交换变量
void Swap(int* a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

//三值取中
int GetMidIndex(int* arr, int left, int right)
{
	int mid = (left + right) / 2;

	if (arr[left] < arr[right])
	{
		if (arr[mid] < arr[left])
		{
			return left;
		}
		else if (arr[mid] < arr[right])
		{
			return mid;
		}
		else
		{
			return right;
		}
	}
	else
	{
		if (arr[mid] < arr[right])
		{
			return right;
		}
		else if (arr[mid] < arr[left])
		{
			return mid;
		}
		else
		{
			return left;
		}
	}
}

//挖坑法单趟排序
int PartSort(int* arr, int left, int right)
{
	//获取基准值，并与left交换位置
	int key = GetMidIndex(arr, left, right);
	Swap(&arr[key], &arr[left]);

	key = arr[left];
	int hole = left;

	while (left < right)
	{
		while (left < right && arr[right] >= key)
		{
			right--;
		}
		arr[hole] = arr[right];
		hole = right;

		while (left < right && arr[left] <= key)
		{
			left++;
		}
		arr[hole] = arr[left];
		hole = left;
	}

	arr[hole] = key;
	return hole;
}

//快速排序递归实现
void QuickSort(int* arr, int begin, int end)
{
	if (begin >= end)
	{
		return;
	}

	int key = PartSort(arr, begin, end);	//单趟排序并获取基准值
	QuickSort(arr, begin, key - 1);	//排左序列
	QuickSort(arr, key + 1, end);		//排右序列
}



int main()
{
	srand((unsigned int)time(NULL));
	int n = 0;
	printf("请输入数组的元素个数:");
	scanf("%d", &n);

	int* arr = (int*)malloc(sizeof(int) * n);
	if (arr == NULL)
	{
		printf("malloc faild\n");
		return 1;
	}

	for (int i = 0; i < n; i++)
	{
		arr[i] = rand() % 10000;
	}

	int begin1 = clock();
	InsertSort(arr, n);
	int end1 = clock();
	printf("插入排序耗时:%d\n", end1 - begin1);

	int begin2 = clock();
	QuickSort(arr, 0, n-1 );
	int end2 = clock();
	printf("快速排序耗时:%d\n", end2 - begin2);
}
