#pragma once

#include <functional>
#include <signal.h>
#include "socket.hpp"
#include "log.hpp"

using func_t = function<string(string& package)>;

class Tcpserver
{
public:
    Tcpserver(uint16_t port, func_t callback)
        : _port(port),
        _callback(callback)
    {
    }
    ~Tcpserver() {}

public:
    bool Initserver()
    {
        _listensock.Scoket();
        _listensock.Bind(_port);
        _listensock.Listen();

        return true;
    }

    void Start()
    {
        signal(SIGPIPE, SIG_IGN);
        signal(SIGCHLD, SIG_IGN);
        cout << "开始提供服务" << endl;
        while (true)
        {
            string clientip;
            uint16_t clientport;
            int sockfd = _listensock.Accept(&clientip, &clientport);
            if (sockfd < 0)
                continue;
            cout << "accept success" << endl;
            // 提供服务
            if (fork() == 0)
            {
                _listensock.Close();
                string inbuffer;
                while (true)
                {
                    char buffer[128];
                    ssize_t n = read(sockfd, buffer, sizeof(buffer));
                    if (n > 0)
                    {
                        buffer[n] = 0;
                        inbuffer += buffer;
                        // cout << "开始计算。。。" << endl;
                        // // string info = _callback(inbuffer);
                        // cout << "tcpserver debug: " << inbuffer << endl;
                        // // write(sockfd, info.c_str(), info.size());
                        // write(sockfd, inbuffer.c_str(), inbuffer.size());
                        while (true)
                        {
                            std::string info = _callback(inbuffer);
                            if (info.empty())
                                break;
                            lg(Debug, "debug, response:\n%s", info.c_str());
                            // lg(Debug, "debug:\n%s", inbuffer.c_str());
                            write(sockfd, info.c_str(), info.size());
                        }
                    }
                    else
                        break;
                }
                exit(0);
            }

            close(sockfd);
        }
    }

private:
    uint16_t _port;
    Sock _listensock;
    func_t _callback;
};