#include <stdio.h>  
#include <stdlib.h>
#include <stdbool.h>

#define MAX_VERTICES 100  // 最大顶点数  
#define MAX_DEGREE 100    // 最大度数（即每个顶点的最大邻接点数）  

// 邻接表节点  
typedef struct AdjListNode {
    int dest;
    struct AdjListNode* next;
} AdjListNode;

// 邻接表  
typedef struct AdjList {
    AdjListNode* head;  // 邻接表头指针  
} AdjList;

// 图  
typedef struct Graph {
    int V;              // 顶点数  
    AdjList* array;     // 邻接表数组  
} Graph;

// 创建一个新的邻接表节点  
AdjListNode* newAdjListNode(int dest) {
    AdjListNode* newNode = (AdjListNode*)malloc(sizeof(AdjListNode));
    newNode->dest = dest;
    newNode->next = NULL;
    return newNode;
}

// 创建一个图  
Graph* createGraph(int V) {
    Graph* graph = (Graph*)malloc(sizeof(Graph));
    graph->V = V;

    // 创建邻接表数组  
    graph->array = (AdjList*)malloc(V * sizeof(AdjList));
    for (int i = 0; i < V; ++i) {
        graph->array[i].head = NULL;
    }

    return graph;
}

// 添加边  
void addEdge(Graph* graph, int src, int dest) {
    // 添加到源顶点的邻接表  
    AdjListNode* newNode = newAdjListNode(dest);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;

    // 因为是无向图，所以也要添加到目标顶点的邻接表  
    newNode = newAdjListNode(src);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}

// 深度优先遍历的辅助函数  
void DFSUtil(Graph* graph, int v, bool visited[]) {
    // 标记当前节点为已访问  
    visited[v] = true;
    printf("%d ", v);

    // 遍历当前节点的所有邻接节点  
    AdjListNode* pCrawl = graph->array[v].head;
    while (pCrawl) {
        if (!visited[pCrawl->dest]) {
            DFSUtil(graph, pCrawl->dest, visited);
        }
        pCrawl = pCrawl->next;
    }
}

// 深度优先遍历函数  
void DFS(Graph* graph, int v) {
    // 标记所有顶点为未访问  
    bool visited[MAX_VERTICES] = { false };

    // 调用递归辅助函数进行深度优先遍历  
    DFSUtil(graph, v, visited);
}

int main() {
    // 创建一个图，包含5个顶点  
    Graph* graph = createGraph(5);
    addEdge(graph, 0, 1);
    addEdge(graph, 0, 4);
    addEdge(graph, 1, 2);
    addEdge(graph, 1, 3);
    addEdge(graph, 1, 4);
    addEdge(graph, 2, 3);
    addEdge(graph, 3, 4);

    printf("深度优先遍历（从顶点 2 开始）:\n");
    DFS(graph, 2);  // 从顶点2开始进行深度优先遍历  

    return 0;
}