#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>


using namespace std;

int main()
{
    string input = "0000000010101234567890001000000004CMBC";
    string ret;
    int j = 10;
    int i = 0;
    int k = 1;
    int len = 0;
    while (j+1 < input.size())
    {

        if (input[i] == '1')
        {
            if (k != 9)
            {
                string tmp;
                tmp += input[j];
                if(j+1<input.size())tmp += input[j + 1];
                j += 2;
                ret += tmp;
                int num =stoi(tmp);
                ret += input.substr(j, num);
                j += num;
            }
            else
            {
                string tmp;
                tmp += input[j];
                if (j + 1 < input.size())tmp += input[j + 1];
                len = stoi(tmp) + 2;
                j += len;
                ret += "09CMBC95568";
            }
        }
        else if (input[i] == '0' && k == 9)
        {
            string tmp;
            tmp += input[j];
            if (j + 1 < input.size())tmp += input[j + 1];
            len = stoi(tmp) + 2;
            j += len;
            ret += "09CMBC95568";
        }
        i++;
        k++;
        if (k % 10 == 0)
        {
            i = j;
            j += 10;
            k = 1;
        }
    }

    while(k < 10&&i<input.size())
    {
        if (k == 9 && input[i] == '0')
            ret += "09CMBC95568";
        k++;
        i++;
    }

    std::cout << ret << endl;
    //cout << ret << endl;
    return 0;
}
