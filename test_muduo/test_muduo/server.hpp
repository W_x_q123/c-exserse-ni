#include <iostream>
#include <vector>
#include <string>
#include <cassert>
#include <cstring>

using namespace std;

#define BUFFER_DEFAULT_SIZE 5

#define INF 0
#define DBG 1
#define ERR 2

#define LOG_LEVEL INF
#define LOG(level, format, ...)                                                                      \
    do                                                                                               \
    {                                                                                                \
        if (level < LOG_LEVEL)                                                                       \
            break;                                                                                   \
        time_t t = time(NULL);                                                                       \
        struct tm *tinfo = localtime(&t);                                                            \
        char tmp[32] = {0};                                                                          \
        strftime(tmp, 31, "%H:%M:%S", tinfo);                                                        \
        fprintf(stdout, "[%s][%s : %d]" format "\n", tmp, __FILE__, __LINE__, format, ##__VA_ARGS__); \
    } while (0)

#define INF_LOG(format, ...) LOG(INF, format, ##__VA_ARGS__)
#define DBG_LOG(format, ...) LOG(DBG, format, ##__VA_ARGS__)
#define ERR_LOG(format, ...) LOG(ERR, format, ##__VA_ARGS__)

class Buffer
{
public:
    std::vector<char> _buffer; // 使用vector作为缓冲区
    uint64_t _reader_idx;      // 读偏移
    uint64_t _writer_idx;      // 写偏移
public:
    Buffer() : _reader_idx(0), _writer_idx(0), _buffer(BUFFER_DEFAULT_SIZE) {}

    char* Begin()
    {
        return &*_buffer.begin();
    }
    // 获取当前写入起始地址
    char* WritePosition()
    {
        return Begin() + _writer_idx;
    }
    // 获取当前读取起始地址
    char* ReadPosition()
    {
        return Begin() + _reader_idx;
    }
    // 获取缓冲区末尾空闲空间大小--写偏移之后的空间
    uint64_t TailIdleSize()
    {
        return _buffer.size() - _writer_idx;
    }
    // 获取缓冲区头部空闲空间大小--读偏移之前的空间
    uint64_t HeadIdleSize()
    {
        return _reader_idx;
    }
    // 获取可读数据大小
    uint64_t ReadableSize()
    {
        return _writer_idx - _reader_idx;
    }
    // 将读偏移向后移动
    void MoveReadOffset(uint64_t len)
    {
        assert(_reader_idx + len <= ReadableSize());

        _reader_idx += len;
    }
    // 将写偏移向后移动
    void MoveWriteOffset(uint64_t len)
    {
        //assert(_writer_idx + len <= TailIdleSize());
        EnsureWriteSpace(len);
        _writer_idx += len;
    }
    // 确保可写空间足够（足够则移动数据，不够则扩容）
    void EnsureWriteSpace(uint64_t len)
    {
        if (len <= TailIdleSize())
            return;
        if (len > TailIdleSize() && len <= (HeadIdleSize() + TailIdleSize())) // 移动数据
        {
            uint64_t rsz = ReadableSize();
            std::copy(ReadPosition(), ReadPosition() + rsz, Begin());
            _reader_idx = 0;
            _writer_idx = rsz;
        }
        else // 扩容
        {
            _buffer.resize(_buffer.size() + len);
        }
    }
    // 写入数据
    void Write(const void* data, uint64_t len)
    {
        // 1.确保可写空间足够
        EnsureWriteSpace(len);
        // 2.写入数据
        std::copy((char*)data, (char*)data + len, WritePosition());
    }

    void WriteAndPush(const void* data, uint64_t len)
    {
        Write(data, len);
        MoveWriteOffset(len);
    }

    void WriteString(const std::string& data)
    {
        Write(&data[0], data.size());
    }

    void WriteStringAndPush(const std::string& data)
    {
        // cout << "before:" << _buffer.size() << endl;
        WriteString(data);
        // cout << "after:" << _buffer.size() << endl;
        MoveWriteOffset(data.size());
    }

    void WriteBuffer(Buffer& data)
    {
        Write(data.ReadPosition(), data.ReadableSize());
    }

    void WriteBufferAndPush(Buffer& data)
    {

        WriteBuffer(data);

        MoveWriteOffset(data.ReadableSize());
    }

    // 读取数据
    void Read(void* buf, uint64_t len)
    {
        // 1.确保可读数据足够
        assert(len <= ReadableSize());
        // 2.读取数据
        std::copy(ReadPosition(), ReadPosition() + len, (char*)buf);
    }

    void ReadAndPop(void* buf, uint64_t len)
    {
        Read(buf, len);
        MoveReadOffset(len);
    }
    std::string ReadString(uint64_t len)
    {
        assert(len <= ReadableSize());
        std::string str;
        str.resize(len);
        Read(&str[0], len);
        return str;
    }

    std::string ReadStringAndPop(uint64_t len)
    {
        std::string str = ReadString(len);
        MoveReadOffset(len);
        return str;
    }

    // 寻找换行符
    char* FindCRLF()
    {
        void* res = memchr(ReadPosition(), '\n', ReadableSize());
        return (char*)res;
    }

    // 获取一行数据
    std::string GetLine()
    {
        char* pos = FindCRLF();
        if (pos == nullptr)
            return "";
        return ReadString(pos - ReadPosition() + 1);
    }

    std::string GetLineAndPop()
    {
        std::string str = GetLine();
        MoveReadOffset(str.size());
        return str;
    }

    // 清除数据
    void Clear()
    {
        _writer_idx = 0;
        _reader_idx = 0;
    }
};