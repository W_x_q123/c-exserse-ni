#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>


using namespace std;


//vector<vector<string>> groupAnagrams(vector<string>& strs)
//{
//    unordered_map<string, vector<string>> hash;
//    for (int i = 0; i < strs.size(); i++)
//    {
//        string tmp = strs[i];
//        sort(tmp.begin(), tmp.end());
//        hash.insert({ tmp,vector<string>().push_back(strs[i]) });
//    }
//
//    vector<vector<string>> ret;
//    auto it = hash.begin();
//    while (it != hash.end())
//    {
//        ret.push_back(it->second);
//    }
//
//    return ret;
//}
//
//
//int main()
//{
//
//}


string ismos(int left, int right, const string& str)
{
    string ret;
    while (left >= 0 && right < str.size() && str[left] == str[right])
    {
        left--;
        right++;
    }

    if (left >= 0 && right < str.size()&&str[left+1]==str[right-1])
        return str.substr(left+1, right-1);
    else
        return  "";
}


string Max(const string& s1, const string& s2)
{
	if (s1.size() > s2.size())
		return s1;
	else
		return s2;
}

string longestPalindrome(string s)
{
    string ret;
    for (int i = 0; i < s.size(); i++)
    {
        string tmp1 = ismos(i, i, s);
        string tmp2 = ismos(i, i + 1, s);

        ret = Max(tmp1, Max(tmp2, ret));
    }

    return ret;
}


//int main()
//{
//	//string str1 = "0123456789";
//	//string str2 = "1234";
//	//string str3 = "1234567";
//	////cout << str.substr(0, 3) << endl;
//
//	//string ret = Max(str1, Max(str2, str3));
//	//cout << ret << endl;
//
//    string s = "babad";
//    cout << longestPalindrome(s) << endl;
//
//	return 0;
//}


int main()
{
    cout << 1+'1' << endl;
    cout << 1+('1' - '0') << endl;

    return 0;
}



class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs)
    {
        unordered_map<string, vector<string>> hash;
        for (int i = 0; i < strs.size(); i++)
        {
            string tmp = strs[i];
            sort(tmp.begin(), tmp.end());
            hash[tmp].push_back(strs[i]);
        }

        vector<vector<string>> ret;
        // auto it = hash.begin();
        // while (it != hash.end())
        // {
        //     ret.push_back(it->second);
        //     it++;
        // }
        for (auto& [x, y] : hash)
        {
            ret.push_back(y);

        }

        return ret;
    }

};


class Solution {
public:
    string longestCommonPrefix(vector<string>& strs)
    {
        int k = 0;
        string ret;
        while (true)
        {
            char ch = strs[0][k];
            int flag = 0;
            for (int j = 0; j < strs.size(); j++)
            {
                if (k >= strs[j].size() || strs[j][k] != ch)
                {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0)
                ret += ch;
            else
                break;
            k++;
        }

        return ret;
    }
};


class Solution {
public:
    string longestPalindrome(string s)
    {
        int begin = 0, len = 0, n = s.size();
        for (int i = 0; i < n; i++)
        {
            int left = i, right = i;
            while (left >= 0 && right < n && s[left] == s[right])
            {
                left--;
                right++;
            }

            if (right - left - 1 > len)
            {
                begin = left + 1;
                len = right - left - 1;
            }

            left = i, right = i + 1;
            while (left >= 0 && right < n && s[left] == s[right])
            {
                left--;
                right++;
            }

            if (right - left - 1 > len)
            {
                begin = left + 1;
                len = right - left - 1;
            }
        }

        return s.substr(begin, len);
    }
};

class Solution {
public:
    string addBinary(string a, string b)
    {
        int t = 0;
        string ret;
        int i = a.size() - 1, j = b.size() - 1;
        while (i >= 0 || j >= 0 || t != 0)
        {
            if (i >= 0)
            {
                t = t + (a[i] - '0');
                i--;
            }

            if (j >= 0)
            {
                t = t + (b[j] - '0');
                j--;
            }

            ret += to_string(t % 2);
            t /= 2;
        }

        reverse(ret.begin(), ret.end());

        return ret;
    }
};