#include <iostream>
#include <string>
#include <vector>

using namespace std;

class HashTable 
{
public:
    HashTable(int size)
        : table(size, ""), count(0)
    {}

    void insert(const string& name) 
    {
        if (find(name)) 
        {
            deleteName(name);
        }
        else 
        {
            if (count == table.size()) 
            {
                resize();
            }
            int index = hashFunction(name);
            while (!table[index].empty()) {
                index = (index + 1) % table.size();
            }
            table[index] = name;
            count++;
        }
    }

    bool find(const string& name) 
    {
        int index = hashFunction(name);
        while (!table[index].empty()) 
        {
            if (table[index] == name) 
            {
                return true;
            }
            index = (index + 1) % table.size();
        }
        return false;
    }

    void deleteName(const string& name) 
    {
        int index = hashFunction(name);
        while (!table[index].empty()) 
        {
            if (table[index] == name) 
            {
                table[index] = "";
                count--;
                break;
            }
            index = (index + 1) % table.size();
        }
    }

private:
    int hashFunction(const string& name) {
        int sum = 0;
        for (char c : name) {
            sum += c;
        }
        return sum % table.size();
    }

    void resize() {
        vector<string> newTable(table.size() * 2, "");
        for (const string& name : table) {
            if (!name.empty()) {
                int index = hashFunction(name);
                while (!newTable[index].empty()) {
                    index = (index + 1) % newTable.size();
                }
                newTable[index] = name;
            }
        }
        table = newTable;
    }

    vector<string> table;
    int count;
};

int main() {
    HashTable hashTable(20);
    hashTable.insert("knd");
    hashTable.insert("mfy");
    hashTable.insert("ena");
    hashTable.insert("mzk");
    hashTable.insert("ick");
    hashTable.insert("saki");
    hashTable.insert("hnm");
    hashTable.insert("shiho");
    hashTable.insert("mnr");
    hashTable.insert("hrk");
    hashTable.insert("airi");
    hashTable.insert("szk");
    hashTable.insert("khn");
    hashTable.insert("an");
    hashTable.insert("akt");
    hashTable.insert("tya");
    hashTable.insert("tks");
    hashTable.insert("emu");

    cout << "查找knd： " << (hashTable.find("knd") ? "存在" : "不存在") << endl;
    hashTable.deleteName("knd");
    cout << "删除knd后查找knd： " << (hashTable.find("knd") ? "存在" : "不存在") << endl;
    hashTable.insert("knd");
    cout << "重新插入knd后查找knd： " << (hashTable.find("knd") ? "存在" : "不存在") << endl;

    return 0;
}



//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
//#define TABLE_SIZE 20
//#define NAME_LEN 50
//
//typedef struct {
//    char name[NAME_LEN];
//} Person;
//
//typedef struct {
//    Person data[TABLE_SIZE];
//    int count;
//} HashTable;
//
//int hash(const char* name) {
//    int sum = 0;
//    for (int i = 0; name[i] != '\0'; i++) {
//        sum += name[i];
//    }
//    return sum % TABLE_SIZE;
//}
//
//void insert(HashTable* table, const char* name) {
//    int index = hash(name);
//    while (table->data[index].name[0] != '\0' && strcmp(table->data[index].name, name) != 0) {
//        index = (index + 1) % TABLE_SIZE;
//    }
//    strcpy(table->data[index].name, name);
//    table->count++;
//}
//
//void delete(HashTable* table, const char* name) {
//    int index = hash(name);
//    while (table->data[index].name[0] != '\0') {
//        if (strcmp(table->data[index].name, name) == 0) {
//            memset(&table->data[index], 0, sizeof(Person));
//            table->count--;
//            break;
//        }
//        index = (index + 1) % TABLE_SIZE;
//    }
//}
//
//int find(HashTable* table, const char* name) {
//    int index = hash(name);
//    while (table->data[index].name[0] != '\0') {
//        if (strcmp(table->data[index].name, name) == 0) {
//            return index;
//        }
//        index = (index + 1) % TABLE_SIZE;
//    }
//    return -1;
//}
//
//int main() {
//    HashTable table = { 0 };
//    char names[20][NAME_LEN] = { "一", "二", "三", "四", "五", "六", "七", "八", "Ivy", "Jack", "Kathy", "Leo", "Mandy", "Nancy", "Oscar", "Paul", "Quincy", "Rachel", "Steve", "Tina" };
//
//    for (int i = 0; i < 20; i++) {
//        insert(&table, names[i]);
//    }
//
//    char search_name[] = "David";
//    int index = find(&table, search_name);
//    if (index != -1) 
//    {
//        printf("Found %s at index %d", search_name, index);
//            delete(&table, search_name);
//    }
//    else {
//        printf("%s not found, inserting it into the table", search_name);
//            insert(&table, search_name);
//    }
//
//    return 0;
//}
