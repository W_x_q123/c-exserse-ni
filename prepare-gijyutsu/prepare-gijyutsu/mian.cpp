#include <iostream>
#include <vector>
#include <string>
#include <cassert>
#include <map>
#include <set>
#include <sstream>
#include <unordered_map>
#include <algorithm>

using namespace std;


//shared_ptr �Զ���
template <class T>
class sharedPtr
{
private:
	size_t* m_cnt;
	T* m_ptr;
public:
	sharedPtr() :m_ptr(nullptr), m_cnt(nullptr) {};
	sharedPtr(T* ptr) :m_ptr(ptr), m_cnt(new size_t(1)) {};

	~sharedPtr()
	{
		if(m_cnt)
			--(*m_cnt);
		if (*m_cnt == 0)
		{
			delete m_ptr; m_ptr = nullptr;
			delete m_cnt; m_cnt = nullptr;
		}
	}

	sharedPtr(const sharedPtr& ptr)
	{
		m_ptr = ptr.m_ptr;
		m_cnt = ptr.m_cnt;
		if(m_cnt) (*m_cnt)++;
	}

	sharedPtr& operator=(const sharedPtr& ptr)
	{
		if (*this != ptr)
		{
			if (m_cnt && --(*m_cnt) == 0)
			{
				delete m_ptr;
				delete m_cnt;
			}
			m_ptr = ptr.m_ptr;
			m_cnt = ptr.m_cnt;
			if (m_cnt)
				++(*m_cnt);
		}
		return *this;
	}

	sharedPtr(sharedPtr&& ptr) noexcept
	{
		m_ptr = ptr.m_ptr;
		m_cnt = ptr.m_cnt;
		ptr.m_ptr = nullptr;
		ptr.m_cnt = nullptr;
	}

	sharedPtr& operator=(sharedPtr&& ptr) noexcept
	{
		if (*this != ptr)
		{
			if (m_cnt && --(*m_cnt) == 0)
			{
				delete m_ptr;
				delete m_cnt;
			}
			m_ptr = ptr.m_ptr; ptr.m_ptr = nullptr;
			m_cnt = ptr.m_cnt; ptr.m_ptr = nullptr;
		}
		return *this;
	}

	size_t use_count()
	{
		return *m_cnt;
	}

	T& operator*()
	{
		return *m_ptr;
	}

	T* operator->()
	{
		return m_ptr;
	}

	operator bool()
	{
		return m_ptr == nullptr;
	}

	bool unique()
	{
		return *m_cnt == 1;
	}

	T* get()
	{
		return *m_ptr;
	}
};


char* my_strcpy(char* dest, const char* src)
{
	assert(dest);
	assert(src);
	char* ret = dest;
	while (*src != '\0')
	{
		/**dest = *src;
		dest++;
		src++;*/
		memmove(dest, src, 1);
		dest++; src++;
	}
	*dest = '\0';
	return ret;
}

//ð������
void bubbleSort(vector<int>& nums)
{
	int n = nums.size();
	if (n <= 1)
		return;
	for (int i = 1; i < n; i++)
	{
		bool isswap = false;
		for (int j = 1; j < n - i + 1; j++)
		{
			if (nums[j] < nums[j - 1])
			{
				swap(nums[j], nums[j - 1]);
				isswap = true;
			}
		}
		if (!isswap)
			break;
	}
}

//��������
//void insertSort(vector<int>& nums)
//{
//	int n = nums.size();
//	for (int i = 0; i < n; ++i)
//	{
//		for (int j = i; j > 0 && nums[j] < nums[j - 1]; --j)
//			swap(nums[j], nums[j - 1]);
//	}
//}

void insertSort(vector<int>& nums)
{
	int n = nums.size();
	for (int i = 0; i < n; i++)
	{
		for (int j = i; j > 0 && nums[j] < nums[j - 1]; --j)
			swap(nums[j], nums[j - 1]);
	}
}

//ϣ������
void shellSort(vector<int>& nums)
{
	int n = nums.size();
	for (int gap = n / 2; gap > 0; gap /= 2)
	{
		for (int i = gap; i < n; ++i)
		{
			for (int j = i; j >=gap  && nums[j] < nums[j - gap]; j -= gap)
				swap(nums[j], nums[j - gap]);
		}
	}
}

//void shellSort(vector<int>& nums)
//{
//	int n = nums.size();
//	for (int gap = n / 2; gap > 0; gap /= 2)
//	{
//		for (int i = gap; i < n; ++i)
//		{
//			for (int j = i; j >= gap && nums[j] < nums[j - gap]; j -= gap)
//				swap(nums[j], nums[j - gap]);
//		}
//	}
//}

//ѡ������
void chooseSort(vector<int>& nums)
{
	int n = nums.size();
	if (n <= 1)
		return;
	int mid = 0;
	for (int i = 0; i < n-1; i++)
	{
		mid = i;
		for (int j = i + 1; j < n; j++)
		{
			if (nums[j] < nums[mid])
				mid = j;
		}
		swap(nums[mid], nums[i]);
	}
}


//void chooseSort(vector<int>& nums)
//{
//	int n = nums.size();
//	for (int i = 0; i < n-1; ++i)
//	{
//		int mid = i;
//		for (int j = i+1; j < n; ++j)
//		{
//			if (nums[j] < nums[mid])
//				mid = j;
//		}
//		swap(nums[i], nums[mid]);
//	}
//}

//��������
int getRandom(vector<int>& nums, int left, int right)
{
	int random = rand();
	return nums[random % (right - left + 1) + left];
}

void quickSort(vector<int>& nums,int l,int r)
{
	if (l >= r)
		return;
	int key = getRandom(nums, l, r);
	int i = l, left = l - 1, right = r + 1;
	while (i < right)
	{
		if (nums[i] < key)
			swap(nums[++left], nums[i++]);
		else if (nums[i] == key)
			i++;
		else
			swap(nums[--right], nums[i]);
	}
	quickSort(nums, l, left);
	quickSort(nums, right, r);
}

//�鲢����
void merge(vector<int>& nums, int left,int mid, int right)
{
	if (left >= right)
		return;
	int* tmp = new int[right-left+1];
	int i = left, j = mid + 1;
	int k = 0;
	while (i <= mid && j <= right)
	{
		if (nums[i] < nums[j])
			tmp[k++] = nums[i++];
		else
			tmp[k++] = nums[j++];
	}

	while(i<=mid)
		tmp[k++] = nums[i++];
	while(j<=right)
		tmp[k++] = nums[j++];

	for (int p = 0; p < k; p++)
		nums[p + left] = tmp[p];
	delete []tmp;
}

void mergeSort(vector<int>& nums, int left, int right)
{
	if (left >= right)
		return;
	int mid = left + (right - left )/2;
	mergeSort(nums, left, mid);
	mergeSort(nums, mid + 1, right);
	merge(nums, left, mid, right);
}


//void merge(vector<int>& nums, int left,int mid, int right)
//{
//	if (left >= right)
//		return;
//	int* tmp = new int[right - left + 1];
//	int i = left, j = mid + 1, k = 0;
//	while (i <= mid && j <= right)
//	{
//		if (nums[i] < nums[j])
//			tmp[k++] = nums[i++];
//		else
//			tmp[k++] = nums[j++];
//	}
//
//	while(i<=mid)
//		tmp[k++] = nums[i++];
//	while(j<=right)
//		tmp[k++] = nums[j++];
//
//	for (int p = 0; p < k; p++)
//		nums[left + p] = tmp[p];
//	delete[]tmp;
//}
//
//void mergeSort(vector<int>& nums, int left, int right)
//{
//	if (left >= right)
//		return;
//	int mid = left + (right - left) / 2;
//	mergeSort(nums, left, mid);
//	mergeSort(nums, mid + 1, right);
//
//	merge(nums, left, mid, right);
//}


void adjustDown(vector<int>& nums, int parent, int n)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && nums[child + 1] > nums[child])
			++child;
		if (nums[child] > nums[parent])
		{
			swap(nums[child], nums[parent]);
			parent = child;
			child = 2 * parent + 1;
		}
		else
			break;
	}
}

//void ad(vector<int>& nums, int parent, int n)
//{
//	int child = 2 * parent + 1;
//	while (child < n)
//	{
//		if (child + 1 < n && nums[child + 1] > nums[child])
//			++child;
//		if (nums[child] > nums[parent])
//		{
//			swap(nums[child], nums[parent]);
//			parent = child;
//			child = 2 * parent + 1;
//		}
//		else
//			break;
//	}
//}
//
//void hs(vector<int>& nums)
//{
//	int n = nums.size();
//	for (int i = (n - 2) / 2; i >= 0; --i)
//	{
//		ad(nums, i, n);
//	}
//
//	for (int i = n - 1; i >= 0; --i)
//	{
//		swap(nums[i], nums[0]);
//		ad(nums, 0, i);
//	}
//}



void heapSort(vector<int>& nums)
{
	int n = nums.size();
	for (int i = (n - 2) / 2; i >= 0; --i)
	{
		adjustDown(nums, i, n);
	}

	for (int i = n-1; i >=0; --i)
	{
		swap(nums[i], nums[0]);
		adjustDown(nums, 0, i);
	}
}



vector<int> func(vector<int>& nums)
{
	int n = nums.size();
	vector<int> result(n, -1);
	multiset<int> ms;

	for (int i = n - 1; i >= 0; --i) {
		auto it = ms.upper_bound(nums[i]);
		if (it != ms.end()) {
			result[i] = *it;
		}
		ms.insert(nums[i]);
	}

	return result;
}
//int main()
//{
//	srand(time(nullptr));
//	/*char str1[] = "mafuyu";
//	char str2[] = "kane";
//	cout << my_strcpy(str1, str2) << endl;*/
//	vector<int> nums = { 3,4,6,5 };
//	//bubbleSort(nums);
//	//insertSort(nums);
//	//shellSort(nums);
//	//chooseSort(nums);
//	//quickSort(nums, 0, nums.size()-1);
//	//mergeSort(nums, 0, nums.size()-1);
//	//heapSort(nums);
//	vector<int> ret = func(nums);
//	for (auto x : ret)
//		cout << x << " ";
//	cout << endl;
//	/*for (auto x : nums)
//		cout << x << " ";
//	cout << endl;*/
//
//	return 0;
//
//}




bool wordPattern(string p, string s)
{
	istringstream iss(s);
	vector<string> words;
	string word;
	while (iss >> word)
		words.emplace_back(word);

	if (p.size() != words.size())
		return false;
	unordered_map<char, string> p2w;
	unordered_map<string, char> w2p;

	for (int i = 0; i < p.size(); i++)
	{
		char c = p[i];
		string w = words[i];

		//���p��w��ӳ��
		if (p2w.count(c) && p2w[c] != w)
			return false;
		//���w��p��ӳ��
		if (w2p.count(w) && w2p[w] != c)
			return false;
		p2w[c] = w;
		w2p[w] = c;
	}
	return true;
}


int nextGreaterElement(int n)
{
	string s = to_string(n);
	if (!next_permutation(s.begin(), s.end()))
		return -1;
	long long ret = stoll(s);

	return ret > INT_MAX ? -1 : (int)ret;
}


vector<int> func1(vector<int>& nums)
{
	int n = nums.size();
	vector<int> ret(n, -1);
	multiset<int> ms;
	for (int i = n - 1; i >= 0; --i)
	{
		auto it = ms.upper_bound(nums[i]);
		if (it != ms.end())
			ret[i] = *it;
		ms.insert(nums[i]);
	}
	return ret;
}


struct Node
{
	Node* left;
	Node* right;
	int val;
	Node (int v):val(v),left(nullptr),right(nullptr){}
};

bool Insert(Node*root,int key)
{
	if (root == nullptr)
	{
		root = new Node(key);
		return true;
	}

	//�ҵ�Ҫ����ĵط�
	Node* parent = nullptr;
	Node* cur = root;
	while (cur)
	{
		if (cur->val > key)
		{
			parent = cur;
			cur = cur->left;
		}

		if (cur->val < key)
		{
			parent = cur;
			cur = cur->right;
		}
		else
			return false;
	}

	//�ж�������߻����ұ�
	cur = new Node(key);
	if (parent->val < key)
		parent->right = cur;
	if (parent->val > key)
		parent->left = cur;
	return true;

}

bool InsertR(Node* root, int key)
{
	if (root == nullptr)
	{
		Node* newnode = new Node(key);
		root = newnode;
		return true;
	}

	if (root->val < key)
		InsertR(root->right, key);
	else if (root->val > key)
		InsertR(root->left, key);
	else
		return false;
}


int main() {
	string s = "hello world this is istringstream";
	istringstream iss(s);
	vector<string> words;
	string word;
	while (iss >> word) {
		words.push_back(word);
	}
	for (auto& w : words)
		cout << w << endl;

	return 0;
}

