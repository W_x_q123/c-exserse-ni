//#include <iostream>
//#include <vector>
//#include <string>
//#include <cassert>
//
//using namespace std;
//
//template<class T>
//class sharedPtr
//{
//private:
//	size_t* m_cnt;
//	T* m_ptr;
//public:
//	sharedPtr():m_ptr(nullptr),m_cnt(nullptr){}
//	sharedPtr(T*ptr):m_ptr(ptr),m_cnt(new size_t(1)){}
//
//	~sharedPtr()
//	{
//		if(m_cnt) --(*m_cnt);
//		if (*m_cnt == 0)
//		{
//			delete m_ptr; m_ptr = nullptr;
//			delete m_cnt; m_cnt = nullptr;
//		}
//	}
//
//	sharedPtr(const sharedPtr& ptr)
//	{
//		m_ptr = ptr.m_ptr;
//		m_cnt = ptr.m_cnt;
//		if (m_cnt)
//			++(*m_cnt);
//	}
//
//	sharedPtr(sharedPtr&& ptr)
//	{
//		m_ptr = ptr.m_ptr;
//		m_cnt = ptr.m_cnt;
//	}
//
//	sharedPtr& operator=(const sharedPtr& ptr)
//	{
//		if (*this != ptr)
//		{
//			if (m_cnt && --(*m_cnt) == 0)
//			{
//				delete m_ptr;
//				delete m_cnt;
//			}
//			m_ptr = ptr.m_ptr;
//			m_cnt = ptr.m_cnt;
//			if (m_cnt)
//				++(*m_cnt);
//		}
//
//		return *this;
//	}
//
//	sharedPtr& operator=(sharedPtr&& ptr)
//	{
//		if (*this != ptr)
//		{
//			if (m_cnt && --(*m_cnt) == 0)
//			{
//				delete m_ptr;
//				delete m_cnt;
//			}
//			m_ptr = ptr.m_ptr;
//			m_cnt = ptr.m_cnt;
//		}
//
//		return *this;
//	}
//
//
//	T& operator*()
//	{
//		if (m_ptr)
//			return *m_ptr;
//		return nullptr;
//	}
//
//	T* operator->()
//	{
//		return m_ptr;
//	}
//
//	operator bool()
//	{
//		return m_ptr == nullptr;
//	}
//
//	T* get()
//	{
//		return m_ptr;
//	}
//
//	size_t use_count()
//	{
//		if (m_cnt)
//			return *m_cnt;
//		return 0;
//	}
//
//	bool unique()
//	{
//		if (m_cnt)
//			return m_cnt == 1;
//		return false;
//	}
//
//
//};
//
//void bubbleSort(vector<int>& nums)
//{
//	int n = nums.size();
//	if (n <= 1)
//		return;
//	for (int i = 1; i < n-1; i++)
//	{
//		bool isswap = false;
//		for (int j = i + 1; j < n - i + 1; j++)
//		{
//			if (nums[j] < nums[j - 1])
//			{
//				isswap = true;
//				swap(nums[j], nums[j - 1]);
//			}
//		}
//		if (isswap == false)
//			break;
//	}
//}
//
//int main()
//{
//	srand(time(nullptr));
//	/*char str1[] = "mafuyu";
//	char str2[] = "kane";
//	cout << my_strcpy(str1, str2) << endl;*/
//	vector<int> nums = { 1,3,5,7,9,2,4,6,8,0 };
//	bubbleSort(nums);
//	//insertSort(nums);
//	//shellSort(nums);
//	//chooseSort(nums);
//	//quickSort(nums, 0, nums.size()-1);
//	//mergeSort(nums, 0, nums.size() - 1);
//	for (auto x : nums)
//		cout << x << " ";
//	cout << endl;
//
//	return 0;
//
//}