#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <memory>
#include <cassert>
#include <cstring>

// 要素
// 1.默认的空间大小
// 2，当前的读取数据位置
// 2，当前的写入数据位置

// 操作
// 1.写入数据
// 当前写入位置指向哪里，就从哪里开始写入
// 如果后续剩余空闲空间不够了
//   1.考虑整体缓冲区空闲空间是否足够(因为读位置也会向后偏移,前边有可能会有空闲空间)
//       足够 : 将数据移动到起始位置即可
//       不够:扩容,从当前写位置开始扩容足够大小
// 数据一旦写入成功，当前写位置，就要向后偏移
// 2.读取数据
// 当前的读取位置指向哪里，就从哪里开始读取，前提是有数据可读
// 可读数据大小:当前写入位置，减去当前读取位置
// 数据一旦读取成功，当前读位置，就要向后偏移

#define DEFAULT_BUFFER_SIZE 1024

class Buffer
{
private:
    std::vector<char> _buffer;
    uint64_t _reader_idx; // 读偏移量
    uint64_t _writer_idx; // 写偏移量

public:
    Buffer(uint64_t size = DEFAULT_BUFFER_SIZE)
        : _buffer(size), _reader_idx(0), _writer_idx(0)
    {
    }

    // 获取缓冲区的起始位置
    char* GetBegin()
    {
        return &(*_buffer.begin());
    }
    // 获取当前的写位置
    char* GetWritePos()
    {
        return GetBegin() + _writer_idx;
    }
    // 获取当前的读位置
    char* GetReadPos()
    {
        return GetBegin() + _reader_idx;
    }
    // 获取缓冲区末尾空闲位置大小
    uint64_t GetTailIdleSize()
    {
        return _buffer.size() - _writer_idx;
    }
    // 获取缓冲区起始空闲位置大小
    uint64_t GetHeadIdleSize()
    {
        return _reader_idx - 0;
    }
    // 获取可读数据大小
    uint64_t GetReadableSize()
    {
        return _writer_idx - _reader_idx;
    }
    // 更新写偏移量
    void MoveWriteOffset(uint64_t len)
    {
        assert(len <= GetTailIdleSize() + GetHeadIdleSize());
        _writer_idx += len;
    }
    // 更新读偏移量
    void MoveReadOffset(uint64_t len)
    {
        assert(len <= GetReadableSize());
        _reader_idx += len;
    }
    // 确保可写空间足够（缓冲区末尾空闲位置大小+缓冲区起始空闲位置大小）
    bool EnsureReadableSize(uint64_t len)
    {
        if (len <= GetTailIdleSize())
            return true;
        if (len <= GetTailIdleSize() + GetHeadIdleSize()) // 如果缓冲区末尾空闲位置大小不够但是缓冲区整体空闲位置足够，那么则把数据移动到缓冲区起始位置处，并更新读便宜和写偏移
        {
            uint64_t rsz = GetReadableSize();                        // 获得可写空间大小
            std::copy(GetReadPos(), GetReadPos() + rsz, GetBegin()); // 把数据移动到缓冲区起始位置
            // 更新读偏移和写偏移
            _reader_idx = 0;
            _writer_idx = rsz;
            return true;
        }
        else // 可写空间不够，开始扩容
        {
            _buffer.resize(_buffer.size() * 2);
            return true;
        }
    }
    // 写数据
    void Write(const void* data, uint64_t len)
    {
        // 1.判断可写空间是否足够
        assert(EnsureReadableSize(len));
        // 2.写入数据
        const char* d = (const char*)data;
        std::copy(d, d + len, GetWritePos());
    }

    // 写入数据并更新写偏移
    void WriteAndPush(const char* data, uint64_t len)
    {
        Write(data, len);
        MoveWriteOffset(len);
    }

    // 写入string
    void WriteString(const std::string& data, uint64_t len)
    {
        Write(data.c_str(), len);
    }

    // 写入string并更新写偏移
    void WriteStringAndPush(const std::string& data, uint64_t len)
    {
        WriteString(data.c_str(), len);
        MoveWriteOffset(len);
    }

    // 写入Buffer
    void WriteBuffer(Buffer& data)
    {
        Write(data.GetBegin(), data.GetReadableSize());
    }

    // 写入Buffer并更新写偏移
    void WriteBufferAndPush(Buffer& data)
    {
        Write(data.GetReadPos(), data.GetReadableSize());
        MoveWriteOffset(data.GetReadableSize());
    }

    // 读数据
    void Read(void* buf, uint64_t len)
    {
        // 1.判断可读空间是否足够
        assert(len <= GetReadableSize());
        // 2.读取数据(把数据读到buf里)
        std::copy(GetReadPos(), GetReadPos() + len, (char*)buf);
    }

    // 读取数据并更新读偏移
    void ReadAndPop(void* buf, uint64_t len)
    {
        Read(buf, len);
        MoveReadOffset(len);
    }

    // 读取string
    std::string ReadString(uint64_t len)
    {
        std::string str;
        str.resize(len);
        Read(&str[0], len); // 因为str.c_str()返回的是const char*类型，所以不能直接赋值给char*类型，所以需要先resize，再赋值
        return str;
    }

    // 读取string并更新读偏移
    std::string ReadStringAndPop(uint64_t len)
    {
        std::string str = ReadString(len);
        MoveReadOffset(len);
        return str;
    }

    // 寻找回车换行符
    char* FindRF()
    {
        char* pos = (char*)memchr(GetReadPos(), '\n', GetReadableSize());
        return pos;
    }

    // 读取一行数据
    std::string ReadLine()
    {
        char* pos = FindRF();
        if (pos == nullptr)
            return "";
        return ReadString(pos - GetReadPos() + 1); //+1 是为了读取回车换行符
    }

    std::string ReadLineAndPop()
    {
        std::string str = ReadLine();
        //std::cout << str;
        MoveReadOffset(str.size());
        return str;
    }

    // 清除数据
    void Clear()
    {
        _writer_idx = 0;
        _reader_idx = 0;
    }
};