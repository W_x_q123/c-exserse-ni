#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

bool containsNearbyDuplicate(vector<int>& nums, int k)
{
    unordered_map<int, int> hash;
    for (int i = 0; i < nums.size(); i++)
    {
        if (hash.count(nums[i]))
        {
            if (abs(i - hash[nums[i]]) <= k)
            {
                cout << "i: " << i << " j: " << hash[nums[i]] << endl;
                return true;
            }
        }
        hash[nums[i]] = i;
    }
    return false;
}


int main()
{
    vector<int> num = { 1,2,3,1,2,3 };
    bool ret = containsNearbyDuplicate(num, 2);

    cout << ret << endl;

    return 0;
}


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    void reorderList(ListNode* head)
    {
        if (head == nullptr || head->next == nullptr || head->next->next == nullptr)
            return;
        //1.找中间节点
        ListNode* fast = head;
        ListNode* slow = head;
        while (fast != nullptr && fast->next != nullptr)
        {
            slow = slow->next;
            fast = fast->next->next;
        }

        ListNode* mid = slow->next;
        slow->next = nullptr;
        //2.逆序后面的部分
        ListNode* tmp = new ListNode(0);
        while (mid)
        {
            ListNode* next = mid->next;
            mid->next = tmp->next;
            tmp->next = mid;
            mid = next;
        }


        //3.合并
        ListNode* cur1 = head;
        ListNode* cur2 = tmp->next;
        ListNode* ret = new ListNode(0);
        ListNode* tail = ret;
        while (cur1 && cur2)
        {
            tail->next = cur1; cur1 = cur1->next;
            tail = tail->next;
            tail->next = cur2; cur2 = cur2->next;
            tail = tail->next;
        }

        if (cur1)
        {
            tail->next = cur1;
        }

        if (cur2)
            tail->next = cur2;

        head = ret->next;
    }
};



/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:

    ListNode* Merge(int left, int right, vector<ListNode*>& lists)
    {
        if (left > right)
            return nullptr;
        if (left == right)
            return lists[left];

        int mid = left + right >> 1;

        ListNode* l1 = Merge(left, mid, lists);
        ListNode* l2 = Merge(mid + 1, right, lists);

        ListNode* cur1 = l1;
        ListNode* cur2 = l2;
        ListNode* ret = new ListNode(0);
        ListNode* tail = ret;
        while (cur1 && cur2)
        {
            if (cur1->val > cur2->val)
            {
                tail->next = cur2;
                tail = tail->next;
                cur2 = cur2->next;
            }
            else
            {
                tail->next = cur1;
                tail = tail->next;
                cur1 = cur1->next;
            }
        }

        if (cur1)
            tail->next = cur1;
        if (cur2)
            tail->next = cur2;
        return ret->next;
    }


    ListNode* mergeKLists(vector<ListNode*>& lists)
    {

        int left = 0, right = lists.size() - 1;

        ListNode* head = new ListNode(0);
        return  Merge(left, right, lists);

    }
};


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k)
    {
        int n = 0;
        ListNode* cur = head;
        while (cur)
        {
            n++;
            cur = cur->next;
        }

        int group = n / k;
        cur = head;
        ListNode* ret = new ListNode(0);
        ListNode* tmp = ret;
        ListNode* prev = ret;
        for (int i = 0; i < group; i++)
        {
            prev = tmp;
            for (int j = 0; j < k; j++)
            {
                ListNode* next = cur->next;
                cur->next = prev->next;
                prev->next = cur;
                if (j == 0) tmp = cur;
                cur = next;
            }
        }
        tmp->next = cur;

        return ret->next;
    }
};


class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target)
    {
        unordered_map<int, int> hash;
        for (int i = 0; i < nums.size(); i++)
        {
            if (hash.count(target - nums[i]))
                return { hash[target - nums[i]],i };
            hash[nums[i]] = i;
        }

        return { -1,-1 };
    }
};


class Solution {
public:
    bool CheckPermutation(string s1, string s2)
    {
        if (s1.size() != s2.size())
            return false;
        int hash[26] = { 0 };
        for (int i = 0; i < s1.size(); i++)
        {
            hash[s1[i] - 'a']++;
        }

        for (int i = 0; i < s2.size(); i++)
        {
            hash[s2[i] - 'a']--;
            if (hash[s2[i] - 'a'] < 0)
                return false;
        }


        return true;
    }
};


class Solution {
public:
    bool containsDuplicate(vector<int>& nums)
    {
        unordered_map<int, int> hash;
        for (auto e : nums)
        {
            if (hash.count(e))
                return true;
            hash[e] = 1;
        }

        return false;
    }
};