#include <iostream>
#include <vector>
#include <string>
#include <cstring>


using namespace std;


int main()
{
	char str1[] = "I saw a Saw saw a Saw !";
	int i = 0;
	while (str1[i] != '\0')
	{
		if (str1[i] >= 'a' && str1[i] <= 'z')
			str1[i] = str1[i] - ('a' - 'A');
		i++;
	}
	char str2[] = "SAW";

	int count = 0;
	int len1 = sizeof(str1) / sizeof(char);
	cout << len1 << endl;
	int len2 = sizeof(str2) / sizeof(char);
	int j = 0;
	while(j<len1)
	{
		char* pch = strstr(str1+j, str2);
		if (pch != NULL)
		{
			count++;
			j += len2;
		}
		else
			break;
	}
	
	cout << count << endl;
	return 0;
}