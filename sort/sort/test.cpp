#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

void Swap(int* x, int* y)
{
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void QuickSort(vector<int> arr, int left, int right)
{
	if (left >= right)
		return;
	int begin = left, end = right;
	int prev = left, cur = left + 1;

	int keyi = left;
	while (cur <= right)
	{
		if (arr[cur] < arr[keyi])
		{
			prev++;
			
			Swap(&arr[cur], &arr[prev]);
			cur++;
		}

		while (arr[cur] > arr[keyi])
		{
			cur++;
		}
	}
	Swap(&arr[keyi], &arr[prev]);
	keyi = prev;
	QuickSort(arr, begin, keyi - 1);
	QuickSort(arr, keyi + 1, end);
}



void Merge(vector<int>& arr, int l, int m, int r) {
    int n1 = m - l + 1;
    int n2 = r - m;
	vector<int> L(n1);
	vector<int> R(n2);

    for (int i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (int j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    int i = 0;
    int j = 0;
    int k = l;

    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        }
        else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void MergeSort(vector<int> arr, int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;

        MergeSort(arr, l, m);
        MergeSort(arr, m + 1, r);

        Merge(arr, l, m, r);
    }
}


int main()
{
	srand(time(NULL));
	int n = 0;
	cout << "请输入数组的元素个数:";
	cin >> n;
	vector<int> arr(n);
	for (int i = 0; i < n; i++)
	{
		arr.push_back(rand() % 10000);
	}

	/*int begin1 = clock();
	QuickSort(arr, 0, arr.size());
	int end1 = clock();
	cout << "快速排序耗时:" << end1 - begin1 << endl;*/

	int begin2 = clock();
	MergeSort(arr, 0, arr.size());
	int end2 = clock();
	cout << "归并排序耗时:" << end2 - begin2 << endl;

	return 0;


}