#include <iostream>
#include <string>

using namespace std;

//string addMax(string s, string t)
//{
//    if (s == "")
//        return "";
//    if (t == "")
//        return "";
//    int tmp = 0;
//    int i = s.size() - 1, j = t.size() - 1;
//    string ret;
//    while (i >= 0 || j >= 0)
//    {
//        int v1 = 0, v2 = 0;
//        if (i >= 0)
//            v1 = s[i--] - '0';
//        if (j >= 0)
//            v2 = t[j--] - '0';
//        ret += to_string((v1 + v2 + tmp) % 10);
//        tmp = (v1 + v2 + tmp) / 10;
//    }
//    if (tmp != 0)
//        ret += to_string(tmp);
//    reverse(ret.begin(), ret.end());
//    return ret;
//}

//string solve(string s, string t) {
//    // write code here
//    int n = 0;
//    if (s == "0" || t == "0")
//        return "0";
//    int i = t.size() - 1;
//    string ret;
//    string tmp2;
//    while (i >= 0)
//    {
//        int v1 = t[i] - '0', v2 = 0, tmp = 0;
//        string tmp1;
//        for (int j = s.size() - 1; j >= 0; j--)
//        {
//            v2 = s[j] - '0';
//            tmp1 += to_string((v1 * v2 + tmp) % 10);
//            tmp = (v1 * v2 + tmp) / 10;
//        }
//        if (tmp != 0)
//            tmp1 += to_string(tmp);
//        reverse(tmp1.begin(), tmp1.end());
//        if (n == 0)
//            tmp2 = "0";
//        else
//        {
//            for (int k = 0; k < n; k++)
//                tmp1 += "0";
//        }
//        tmp2 = addMax(tmp1, tmp2);
//        ret = tmp2;
//        n++;
//        i--;
//
//    }
//    return ret;
//
//}
//
//int main()
//{
//    string s = "999";
//    string t = "999";
//    string ret = solve(s, t);
//    cout << ret << endl;
//    return 0;
//}


struct ListNode {
    int val;
    struct ListNode* next;
    ListNode(int x) : val(x), next(nullptr) {}
};

ListNode* reverseBetween(ListNode* head, int m, int n) {
    // write code here
    ListNode* cur = head;
    ListNode* retHead = new ListNode(0);
    ListNode* tail = retHead;
    int i = 1;
    while (cur != nullptr)
    {

        while (i >= m && i <= n)
        {
            ListNode* tmp = cur;
            ListNode* next = tail->next;
            tmp->next = next;
            tail->next = tmp;
            cur = cur->next;
            i++;
        }
        tail->next = cur;
        tail = tail->next;
        cur = cur->next;
        i++;
    }
    return retHead->next;
}

int main()
{
    ListNode* n1 = new ListNode(1);
    ListNode* n2 = new ListNode(2);
    ListNode* n3 = new ListNode(3);
    ListNode* n4 = new ListNode(4);
    ListNode* n5 = new ListNode(5);

    n1->next = n2; n2->next = n3; n3->next = n4; n4->next = n5;

    reverseBetween(n1, 2, 4);

    return 0;
}