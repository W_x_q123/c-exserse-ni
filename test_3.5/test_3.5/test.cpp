#include <iostream>
#include <string>
#include <vector>
#include <algorithm>


using namespace std;


/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    vector<vector<int>> levelOrder(Node* root)
    {
        vector<vector<int>> ret;
        queue<Node*> q;
        if (root == nullptr)
            return ret;
        q.push(root);
        while (q.size())
        {
            int sz = q.size();
            vector<int> tmp;   //将一行的数据存到这个数组
            for (int i = 0; i < sz; i++)
            {
                Node* t = q.front();  //出队头，进队头的孩子节点
                q.pop();
                tmp.push_back(t->val);
                for (Node* child : t->children)
                {
                    q.push(child);
                }
            }
            ret.push_back(tmp);
        }

        return ret;
    }
};



/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root)
    {
        vector<vector<int>> ret;
        queue<TreeNode*> q;
        if (root == nullptr)
            return ret;
        int level = 1;
        q.push(root);
        while (q.size())
        {
            int sz = q.size();
            vector<int> tmp;
            for (int i = 0; i < sz; i++)
            {
                TreeNode* t = q.front();
                q.pop();
                tmp.push_back(t->val);
                if (t->left) q.push(t->left);
                if (t->right) q.push(t->right);
            }

            if (level % 2 == 1)
                ret.push_back(tmp);
            else
            {
                reverse(tmp.begin(), tmp.end());
                ret.push_back(tmp);
            }
            level++;
        }

        return ret;
    }
};